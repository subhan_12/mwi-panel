<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Waste Info</title>

		<link rel="stylesheet" type="text/css" href="styles/theme.css"/>
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-40855148-1', 'impactapps.com.au');
ga('send', 'pageview');

</script>
	</head>
	<body>
		<div class="wrapper">
			
		<div class="header">
			
			<div class="left_header">
			<a href="index.php"><img src="images/logo.png" /></a>
			</div>	<!--left_header ends-->
			
			<div class="right_header">
				<div class="menu_div">
					<ul class="menu">
				<li class="nav"><a  href="index.php" >Home</a></li>
				<li class="nav"><a href="councils.php" style="color:#1DA442;">Councils</a></li>
				<li class="nav"><a href="resident.php">Residents</a></li>
				<li class="nav" style="width:120px;"><a href="download_app.php" style="width:120px;">Download App</a></li>
				<li class="nav"><a href="about.php">About Us</a></li>
				<li class="nav"><a href="contact.php">Contact Us</a></li>
				
			</ul>
				</div>	<!--menu ends-->
				<div class="header_info">
					<h2 style="">Convenient For Residents</h2>
					<p></p>
					
					
				</div>	<!--header_info ends-->
				
				
			</div>	<!--right_header ends-->
			
		</div>	<!--header ends-->
		
		<div class="container">
			<div class="left_content">
				<div class="left_menu_div">
					<ul class="left-nav">

							<li class="left-list" style="background: url(images/green_bar2.png);width:210px;">
								<a  style="text-decoration:none;color:#fff;width:70px;hover:background:none;" href="index.php" onmouseover="this.style.background = 'none'">Home</a>
<span style="margin-left:-30px;display:inline-block;width:30px;color:#fff;">></span>
<a href="councils.php" style="width:70px;text-decoration:underline;color:#fff;margin-left:-30px;" onmouseover="this.style.background = 'none'">Councils</a>
							</li>
<li class="left-list" >
								<a  href="technology_innov.php">> Technology Innovation</a>
							</li>
							<li class="left-list" >
								<a  href="saving_councils.php" >> Saving Your Councils Money</a>
							</li>
							<li class="left-list" >
								<a  href="customized_councils.php" >> Customised To Your Councils </a>
							</li>
							<li class="left-list" >
								<a  href="#" style="background:#949494;color:#fff">> Convenient For Residents</a>
							</li>
							
							<li class="left-list" >
								<a  href="report_dumping.php">> Report Littering & Dumping</a>
							</li>
							
							<li class="left-list" >
								<a  href="enquiry.php">> Submit An Enquiry</a>
							</li>
							
							

						</ul>
				</div>
			</div>	<!--left_content ends-->
			
			
			<div class="center_content" >
				
				<p class="tech_inno" style="padding:5px;margin:0px;padding-left:10px;padding-top:45px;">
					Recent research has shown that smartphone ownership has surged in Australia to over 60% of mobile phone users – and it is only set to grow more! It is estimated that 86% of smartphone users look for local info in the phones, which makes this App ideal for Councils communicating with residents.
					
					
					<br><br>
					
					Smartphones put vital information into the hands of those mostly likely to use it. They are a convenient, fast and cost-effective way for Councils to engage with their residents.<br><br>
					<p class="tech_inno" style="font-weight:bold;padding-bottom:0px;">FREE TO DOWNLOAD</p>
					
					<p class="tech_inno" style="padding-top:0px;">Once purchased for a particular particular Council or Regional Waste area, the Waste Info App is free for residents to download. It is compatible with Apple and Android operating systems on smartphones and tablets. The App is supported with a demonstration video and answers to frequently asked questions.</p>
				
<p class="tech_inno" style="font-weight:bold;padding-bottom:0px;">REPORT A PROBLEM</p>
					
					<p class="tech_inno" style="padding-top:0px;">
						The Waste Info App has a convenient ‘Report a Problem’ function. With a few clicks, residents can report common problems such as missed bins. Residents can easily take a photo of dumped rubbish and send the report to the nominated Council or waste contractor email address.
					</p>
				
				
				
<p class="tech_inno" style="font-weight:bold;padding-bottom:0px;">SET REMINDERS</p>
					
					<p class="tech_inno" style="padding-top:0px;">
						One of the main functions of the Waste Info App is for residents to set alarms or reminders for when garbage, recycling and garden waste services are due to arrive. This is better for Council, the waste contractor and residents as less revisits are required.
						</p>				
				
				
	<p class="tech_inno" style="font-weight:bold;padding-bottom:0px;">BUSINESS HOURS AND LOCATIONS</p>
					
					<p class="tech_inno" style="padding-top:0px;">
						The Waste Info App contains important information relating to the Waste Facilities such as opening hours, locations and maps, disposal costs and any special conditions. This should serve to remove the frustrations of residents arriving at the waste depot with a full load just after closing time or being unaware of costs.
						</p>				
				
			
<p class="tech_inno" style="font-weight:bold;padding-bottom:0px;">GENERAL INFORMATION</p>
					
					<p class="tech_inno" style="padding-top:0px;">
						The Waste Info App contains information on all types of waste (from car tyres to mobile phones), how they can be recycled and what to do with hazardous materials – very handy information for the residents!
						</p>				
			
			
			
			
			
			
			
			
			
			
			
			
				
				
				</p>
				
				
				
			</div>	<!--center_content ends-->
			<div class="right_content">
				
				<img style="margin-top:40px;margin-left:75px;" src="images/iphone_report_a_problem.png">
				<img style="margin-top:10px;margin-left:75px;" src="images/2.png">
				<img style="margin-top:10px;margin-left:75px;" src="images/3.png">
				
			</div>	<!--right_content ends-->
			
			
			
			
		</div>	<!--container ends-->
			
		</div>	<!--wrapper ends-->
		
			
			<div class="footer_div">
					<ul class="menu_foo">
				<li class="nav"><a  href="index.php" style="color:#1DA442;">Home</a></li>
				<li class="nav"><a href="resident_faq.php">FAQs</a></li>
				<li class="nav"><a href="testimonial.php">Testimonials</a></li>
				<li class="nav" style="width:120px;"><a href="privacy.php" style="width:120px;">Privacy Policy</a></li>
				<li class="nav"><a href="enquiry.php" style="width:120px;">Submit An Enquiry</a></li>
				<span class="nav_foo" style="">Copyright &copy; 2013 Impact Apps</span>
				
			</ul>
		</div>
		
	</body>
	</html>