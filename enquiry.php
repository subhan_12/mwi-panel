<?php

$msg = $_GET['msg'];


?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-40855148-1', 'impactapps.com.au');
ga('send', 'pageview');

</script>
		<title>Waste Info</title>

		<link rel="stylesheet" type="text/css" href="styles/theme.css"/>
		<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="js/contact_val.js"></script>

	</head>
	<body>
		<div class="wrapper">
			
		<div class="header">
			
			<div class="left_header">
			<a href="index.php"><img src="images/logo.png" /></a>
			</div>	<!--left_header ends-->
			
			<div class="right_header">
				<div class="menu_div">
					<ul class="menu">
				<li class="nav"><a  href="index.php" >Home</a></li>
				<li class="nav"><a href="councils.php" style="color:#1DA442;">Councils</a></li>
				<li class="nav"><a href="resident.php">Residents</a></li>
				<li class="nav" style="width:120px;"><a href="download_app.php" style="width:120px;">Download App</a></li>
				<li class="nav"><a href="about.php">About Us</a></li>
				<li class="nav"><a href="contact.php">Contact Us</a></li>
				
			</ul>
				</div>	<!--menu ends-->
				<div class="header_info">
					<h2 style="">Submit An Enquiry</h2>
					<p></p>
					
					
				</div>	<!--header_info ends-->
				
				
			</div>	<!--right_header ends-->
			
		</div>	<!--header ends-->
		
		<div class="container">
			<div class="left_content">
				<div class="left_menu_div">
					<ul class="left-nav">

							<li class="left-list" style="background: url(images/green_bar2.png);width:210px;">
								<a  style="text-decoration:none;color:#fff;width:70px;hover:background:none;" href="index.php" onmouseover="this.style.background = 'none'">Home</a>
<span style="margin-left:-30px;display:inline-block;width:30px;color:#fff;">></span>
<a href="councils.php" style="width:70px;text-decoration:underline;color:#fff;margin-left:-30px;" onmouseover="this.style.background = 'none'">Councils</a>
							</li>
<li class="left-list" >
								<a  href="technology_innov.php">> Technology Innovation</a>
							</li>
							<li class="left-list" >
								<a  href="saving_councils.php" >> Saving Your Councils Money</a>
							</li>
							<li class="left-list" >
								<a  href="customized_councils.php" >> Customised To Your Councils </a>
							</li>
							<li class="left-list" >
								<a  href="convenient_residents.php">> Convenient For Residents</a>
							</li>
							
							<li class="left-list" >
								<a  href="report_dumping.php">> Report Littering & Dumping</a>
							</li>
							
							<li class="left-list" >
								<a  style="background:#949494;color:#fff" href="#">> Submit An Enquiry</a>
							</li>
							
							

						</ul>
				</div>
			</div>	<!--left_content ends-->
			
			
			<div class="center_content" >
				
				<p class="tech_inno" style="padding:5px;margin:0px;padding-left:10px;padding-top:45px;">
					Submit an enquiry to Impact Apps.
					<br>
					note * asterisk denotes compulsory information
				</p>
				
				<form name="login_form" class="az-login" id="login" action="email.php" method="POST" onsubmit="return validateForm()">

						<label>Full Name</label><span class="enquiry_star" id="fname">*</span>
						<input type="text" id="flname" name="flname"  class="az-user" onfocus="return fn()"/>
						<br>
						<label>Organisation</label><span class="enquiry_star" id="org">*</span>
						<input type="text" id="org" name="org"  class="az-user"  onfocus="return or()"/>
						<br>
						<label>Email</label><span class="enquiry_star" id="email">*</span>
						<input type="text" id="email" name="email"  class="az-user" onfocus="return em()" />
						<br>
						<label>Phone No</label><span class="enquiry_star" id="phone">*</span>
						<input type="text" id="phone" name="phone"  class="az-user"  onfocus="return ph()"/>
						<br>
						<label>Location</label><span class="enquiry_star" id="loc">*</span>
						<input type="text" id="loc" name="loc"  class="az-user"  onfocus="return lo()"/>
						<br>
						<label style="vertical-align: top">Message</label><span class="enquiry_star" id="msg" style="vertical-align: top";display:inline-block;>*</span>
						<textarea class="az-user_text" id="msg" maxlength = "160" name="msg" placeholder = "Max 150 Characters" onfocus="return m()"/></textarea>
						<br>
						
						<label></label>
						<input type="submit" name="submit" class="az-submit  az-btn" value="Submit">
						<input type="reset" name="reset" class="az-submit classname az-btn" value="Clear">

					</form>
				<br>
				<?php
				if($msg){
					
					echo "<div style='color:green;text-align:center;'>$msg</div>";
					
				}
				
				?>
				
			</div>	<!--center_content ends-->
			<div class="right_content">
				<img style="margin-top:40px;margin-left:55px;" src="images/offer_image.png">
				<a href="docs/brochure.pdf"><img style="margin-top:10px;margin-left:75px;" src="images/download_brochure_icon.png"></a>
				<a href="docs/order_form.docx"><img style="margin-top:20px;margin-left:65px;" src="images/download_order_form_botton.png"></a>
				<a href="resident_video.php"><img style="margin-top:20px;margin-left:65px;" src="images/video_icon.png"></a>
				
			</div>	<!--right_content ends-->
			
			
			
			
		</div>	<!--container ends-->
			
		</div>	<!--wrapper ends-->
		
			
			<div class="footer_div">
					<ul class="menu_foo">
				<li class="nav"><a  href="index.php" style="">Home</a></li>
				<li class="nav"><a href="resident_faq.php">FAQs</a></li>
				<li class="nav"><a href="testimonial.php">Testimonials</a></li>
				<li class="nav" style="width:120px;"><a href="privacy.php" style="width:120px;">Privacy Policy</a></li>
				<li class="nav"><a href="enquiry.php" style="width:120px;color:#1DA442;">Submit An Enquiry</a></li>
				<span class="nav_foo" style="">Copyright &copy; 2013 Impact Apps</span>
				
			</ul>
		</div>
		
	</body>
	</html>