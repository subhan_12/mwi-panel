<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-40855148-1', 'impactapps.com.au');
ga('send', 'pageview');

</script>
<title>Waste Info</title>

		<link rel="stylesheet" type="text/css" href="styles/theme.css"/>

<!--[if IE 9]>

<link rel="stylesheet" type="text/css" href="styles/ie.css"&gt; 

<![endif]-->

	

<script>

//alert(navigator.userAgent);;
</script>

	</head>
	<body>
		<div class="wrapper">
			
		<div class="header">
			
			<div class="left_header">
			<a href="index.php"><img src="images/logo.png" /></a>
			</div>	<!--left_header ends-->
			
			<div class="right_header">
				<div class="menu_div">
					<ul class="menu">
				<li class="nav"><a  href="#" style="color:#1DA442;">Home</a></li>
				<li class="nav"><a href="councils.php">Councils</a></li>
				<li class="nav"><a href="resident.php">Residents</a></li>
				<li class="nav" style="width:120px;"><a href="download_app.php" style="width:120px;">Download App</a></li>
				<li class="nav"><a href="about.php">About Us</a></li>
				<li class="nav"><a href="contact.php">Contact Us</a></li>
				
			</ul>
				</div>	<!--menu ends-->
				<div class="header_info">
					<h2>Impact Apps</h2>
					<p style="">Putting Information about waste management<br>
 services in YOUR hands
</p>
					
					
				</div>	<!--header_info ends-->
				
				
			</div>	<!--right_header ends-->
			
		</div>	<!--header ends-->
		
		<div class="container">
			<div class="left_content">
				<div class="left_menu_div"></div>
			</div>	<!--left_content ends-->
			
			
			<div class="center_content">
				<img src="images/iphone_image.png" >
				<p class="center_content_para" style="margin-top:50px;">

						Introducing the new Waste Info App</p>
						<p class="center_content_para" id="fline"  >It’s a new way to find out about all the</p>
						<p class="center_content_para" id="sline" >waste management services offered by</p>
						<p class="center_content_para" id="tline">your council where you live.</p>
						<p class="center_content_para" id="fvline" >TELL ME MORE ......

					</p>
				
				<span class="councils_link"><a class="center_content_an" href="councils.php">Councils</a>   <a class="center_content_an" href="resident.php">Residents</a></span>
				
				
			</div>	<!--center_content ends-->
			<div class="right_content" style="margin-top:-25px;">
				<img src="images/tablet_image.png" >
			</div>	<!--right_content ends-->
			
			
			
			
		</div>	<!--container ends-->
			
		</div>	<!--wrapper ends-->
		
			
			<div class="footer_div">
					<ul class="menu_foo">
				<li class="nav"><a  href="#" style="color:#1DA442;">Home</a></li>
				<li class="nav"><a href="resident_faq.php">FAQs</a></li>
				<li class="nav"><a href="testimonial.php">Testimonials</a></li>
				<li class="nav" style="width:120px;"><a href="privacy.php" style="width:120px;">Privacy Policy</a></li>
				<li class="nav"><a href="enquiry.php" style="width:120px;">Submit An Enquiry</a></li>
				<span class="nav_foo" style="">Copyright &copy; 2013 Impact Apps</span>
				
			</ul>
		</div>
		
	</body>
	</html>