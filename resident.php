<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-40855148-1', 'impactapps.com.au');
ga('send', 'pageview');

</script>
		<title>Waste Info</title>

		<link rel="stylesheet" type="text/css" href="styles/theme.css"/>

	</head>
	<body>
		<div class="wrapper">
			
		<div class="header">
			
			<div class="left_header">
				<a href="index.php"><img src="images/logo.png" /></a>
			</div>	<!--left_header ends-->
			
			<div class="right_header">
				<div class="menu_div">
					<ul class="menu">
				<li class="nav"><a  href="index.php" >Home</a></li>
				<li class="nav"><a href="councils.php" >Councils</a></li>
				<li class="nav"><a href="#" style="color:#1DA442;">Residents</a></li>
				<li class="nav" style="width:120px;"><a href="download_app.php" style="width:120px;">Download App</a></li>
				<li class="nav"><a href="about.php">About Us</a></li>
				<li class="nav"><a href="contact.php">Contact Us</a></li>
				
			</ul>
				</div>	<!--menu ends-->
				<div class="header_info">
					<h2>Residents</h2>
					<p></p>
					
					
				</div>	<!--header_info ends-->
				
				
			</div>	<!--right_header ends-->
			
		</div>	<!--header ends-->
		
		<div class="container">
			<div class="left_content">
				<div class="left_menu_div">
					<ul class="left-nav">

							<li class="left-list" style="background: url(images/green_bar2.png);width:210px;">
<a  style="text-decoration:none;color:#fff;width:70px;hover:background:none;" href="index.php" onmouseover="this.style.background = 'none'">Home </a>
<span style="margin-left:-30px;display:inline-block;width:30px;color:#fff;">></span>
<a href="resident.php" style="width:70px;text-decoration:underline;color:#fff;margin-left:-30px;" onmouseover="this.style.background = 'none'">Residents</a>
							</li>
							<li class="left-list" >
								<a  href="download_app.php">> Download The App</a>
							</li>
							<li class="left-list" >
								<a  href="resident_video.php">> View Demo Video </a>
							</li>
							<li class="left-list" >
								<a  href="resident_faq.php">> FAQs</a>
							</li>
							
							<li class="left-list" >
								<a  href="resident_link.php">> Links to Councils</a>
							</li>
							
							
							
							

						</ul>
				</div>
			</div>	<!--left_content ends-->
			
			
			<div class="center_content" >
				
				<p class="tech_inno" style="padding:5px;margin:0px;padding-left:10px;padding-top:45px;">
Once you download this FREE App onto your smartphone or tablet you will be able to easily and quickly:					
					<br><br>
					
					
					
					
	<ul class="tech_inno_list" style="list-style-image:url('images/App-clean-icon.png');width:460px;">
					<li><a class="tech_inno_an">Find out what day your garden organics and recycling is collected AND set an alarm to remind you.</a></li>
					<li><a class="tech_inno_an">Locate your nearest waste transfer station or landfill AND find out the cost of disposal.</a></li>
					<li><a class="tech_inno_an">Discover heaps of facts about what you can recycle and what garden organics is.</a></li>
					<li><a class="tech_inno_an">Find out what to do with problem waste materials like chemicals, batteries, computers and so on.</a></li>

					<li><a class="tech_inno_an">Report a problem direct to your Council such as a missed pickup, littering or illegally dumped rubbish. You can even take a photo and send that too!</a></li>
					<li><a class="tech_inno_an">Request a bulky goods kerbside collection.</a></li>
					<li><a class="tech_inno_an">Report fallen branches/trees, graffiti, overflowing public bins, dead animals on roadways.</a></li>
					<li><a class="tech_inno_an">Request a tour of the local Recycling and Waste Management Facilities.</a></li>
				
					
					
					
					</ul><br>				
					
				
					
					
				</p>
				
				<p class="tech_inno" style="width:550px;">ALL THIS DONE FOR FREE THROUGH YOUR SMARTPHONE OR TABLET<br><br><br>Find out more .....</p>
				
				
				
			</div>	<!--center_content ends-->
			<div class="right_content">
				
				<!--<img style="margin-top:30px;margin-left:55px;" src="images/red_basket.png">-->
			</div>	<!--right_content ends-->
			
			
			
			
		</div>	<!--container ends-->
			
		</div>	<!--wrapper ends-->
		
			
			<div class="footer_div">
					<ul class="menu_foo">
				<li class="nav"><a  href="index.php" style="color:#1DA442;">Home</a></li>
				<li class="nav"><a href="resident_faq.php">FAQs</a></li>
				<li class="nav"><a href="testimonial.php">Testimonials</a></li>
				<li class="nav" style="width:120px;"><a href="privacy.php" style="width:120px;">Privacy Policy</a></li>
				<li class="nav"><a href="enquiry.php" style="width:120px;">Submit An Enquiry</a></li>
				<span class="nav_foo" style="">Copyright &copy; 2013 Impact Apps</span>
				
			</ul>
		</div>
		
	</body>
	</html>