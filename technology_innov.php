<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-40855148-1', 'impactapps.com.au');
ga('send', 'pageview');

</script>
		<title>Waste Info</title>

		<link rel="stylesheet" type="text/css" href="styles/theme.css"/>

	</head>
	<body>
		<div class="wrapper">
			
		<div class="header">
			
			<div class="left_header">
			<a href="index.php"><img src="images/logo.png" /></a>
			</div>	<!--left_header ends-->
			
			<div class="right_header">
				<div class="menu_div">
					<ul class="menu">
				<li class="nav"><a  href="index.php" >Home</a></li>
				<li class="nav"><a href="councils.php" style="color:#1DA442;">Councils</a></li>
				<li class="nav"><a href="resident.php">Residents</a></li>
				<li class="nav" style="width:120px;"><a href="download_app.php" style="width:120px;">Download App</a></li>
				<li class="nav"><a href="about.php">About Us</a></li>
				<li class="nav"><a href="contact.php">Contact Us</a></li>
				
			</ul>
				</div>	<!--menu ends-->
				<div class="header_info">
					<h2>Technology Innovation</h2>
					<p></p>
					
					
				</div>	<!--header_info ends-->
				
				
			</div>	<!--right_header ends-->
			
		</div>	<!--header ends-->
		
		<div class="container">
			<div class="left_content">
				<div class="left_menu_div">
					<ul class="left-nav">

							<li class="left-list" style="background: url(images/green_bar2.png);width:210px;">
								<a  style="text-decoration:none;color:#fff;width:70px;hover:background:none;" href="index.php" onmouseover="this.style.background = 'none'">Home</a>
<span style="margin-left:-30px;display:inline-block;width:30px;color:#fff;">></span>
<a href="councils.php" style="width:70px;text-decoration:underline;color:#fff;margin-left:-30px;" onmouseover="this.style.background = 'none'">Councils</a>
							</li>
<li class="left-list" >
								<a  style="background:#949494;color:#fff" href="technology_innov.php">> Technology Innovation</a>
							</li>
							<li class="left-list" >
								<a  href="saving_councils.php">> Saving Your Councils Money</a>
							</li>
							<li class="left-list" >
								<a  href="customized_councils.php">> Customised To Your Councils </a>
							</li>
							<li class="left-list" >
								<a  href="convenient_residents.php">> Convenient For Residents</a>
							</li>
							
							<li class="left-list" >
								<a  href="report_dumping.php">> Report Littering & Dumping</a>
							</li>
							
							<li class="left-list" >
								<a  href="enquiry.php">> Submit An Enquiry</a>
							</li>
							
							

						</ul>
				</div>
			</div>	<!--left_content ends-->
			
			
			<div class="center_content">
				
				<p class="tech_inno" style="padding:5px;margin:0px;padding-left:10px;padding-top:45px;">Technology Innovation and Waste Management<br><br>
					
					The Challenges<br>
					<ul class="tech_inno_list">
					<li><a class="tech_inno_an">Keeping residents up to date with service information, fees and charges</a></li>
					<li><a class="tech_inno_an">Educating adults and children</a></li>
					<li><a class="tech_inno_an">Engaging people to get their feedback on issues</a></li>
					<li><a class="tech_inno_an">Dealing with complaints quickly and efficiently</a></li>
					<li><a class="tech_inno_an">Using relevant and targeted communication tools</a></li>
					
					</ul><br>
					
					<p class="tech_inno">The current methods used to communicate (websites, mail-outs, posters, media ads, newsletters, events, school visits, etc) have their limitations:</p>
					<ul class="tech_inno_list">
					<li><a class="tech_inno_an">Delivery mode is not always integrated</a></li>
					<li><a class="tech_inno_an">Sometimes the emphasis is on the ‘scattered’ approach</a></li>
					<li><a class="tech_inno_an">Can be very resource hungry ($$$ and people)</a></li>
					<li><a class="tech_inno_an">Hard to get good data on effectiveness</a></li>
					
					
					</ul><br>
					
					<p class="tech_inno">There are many benefits of utilising a specialised smartphone or tablet app</p>
					
					<ul class="tech_inno_list">
					<li><a class="tech_inno_an">Over 60% of phones in Australia are now smartphones</a></li>
					<li><a class="tech_inno_an">Once purchased by Council the Waste Info App is free to residents</a></li>
					<li><a class="tech_inno_an">Works on Apple and Android smartphones & tablets (>90% market)</a></li>
					<li><a class="tech_inno_an">Summarises important information on waste services and facilities</a></li>
					<li><a class="tech_inno_an">The App can be updated twice a year, including news and events specific to your council or waste area</a></li>
					<li><a class="tech_inno_an">Opportunity to integrate websites, phone apps, Twitter and Facebook as a means of getting the messages out.</a></li>
					<li><a class="tech_inno_an">The App is cheap to set up</a></li>
					<li><a class="tech_inno_an">It allows councils to collect good, meaningful feedback from residents</a></li>
					<li><a class="tech_inno_an">It allows quick and direct communication with residents</a></li>
					<li><a class="tech_inno_an">They empower the residents to do more</a></li>
					<li><a class="tech_inno_an">They appeal more to the coming generation</a></li>
					
					
					</ul><br>
					
				
					
					<p class="tech_inno">Click on the links in the Navigation Bar at the top left to explore the benefits of the Waste Info App in greater detail.</p>
				</p>
				
				
				
				
				
			</div>	<!--center_content ends-->
			<div class="right_content">
				<img style="margin-top:40px;margin-left:55px;" src="images/offer_image.png">
				<img style="margin-top:30px;margin-left:55px;" src="images/green_basket.png">
				<img style="margin-top:30px;margin-left:55px;" src="images/yellow_basket.png">
				<img style="margin-top:30px;margin-left:55px;" src="images/red_basket.png">
			</div>	<!--right_content ends-->
			
			
			
			
		</div>	<!--container ends-->
			
		</div>	<!--wrapper ends-->
		
			
			<div class="footer_div">
					<ul class="menu_foo">
				<li class="nav"><a  href="index.php" style="color:#1DA442;">Home</a></li>
				<li class="nav"><a href="resident_faq.php">FAQs</a></li>
				<li class="nav"><a href="testimonial.php">Testimonials</a></li>
				<li class="nav" style="width:120px;"><a href="privacy.php" style="width:120px;">Privacy Policy</a></li>
				<li class="nav"><a href="enquiry.php" style="width:120px;">Submit An Enquiry</a></li>
				<span class="nav_foo" style="">Copyright &copy; 2013 Impact Apps</span>
				
			</ul>
		</div>
		
	</body>
	</html>