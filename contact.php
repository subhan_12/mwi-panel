<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Waste Info</title>

		<link rel="stylesheet" type="text/css" href="styles/theme.css"/>
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-40855148-1', 'impactapps.com.au');
ga('send', 'pageview');

</script>
	</head>
	<body>
		<div class="wrapper">
			
		<div class="header">
			
			<div class="left_header">
				<a href="index.php"><img src="images/logo.png" /></a>
			</div>	<!--left_header ends-->
			
			<div class="right_header">
				<div class="menu_div">
					<ul class="menu">
				<li class="nav"><a  href="index.php" >Home</a></li>
				<li class="nav"><a href="councils.php" >Councils</a></li>
				<li class="nav"><a href="resident.php" >Residents</a></li>
				<li class="nav" style="width:120px;"><a href="download_app.php" style="width:120px;">Download App</a></li>
				<li class="nav"><a href="about.php" >About Us</a></li>
				<li class="nav"><a href="contact.php" style="color:#1DA442;">Contact Us</a></li>
				
			</ul>
				</div>	<!--menu ends-->
				<div class="header_info">
					<h2 style="">Contact Us</h2>
					<p></p>
					
					
				</div>	<!--header_info ends-->
				
				
			</div>	<!--right_header ends-->
			
		</div>	<!--header ends-->
		
		<div class="container">
			<div class="left_content">
				<div class="left_menu_div">
					<ul class="left-nav">

							<li class="left-list" style="background: url(images/green_bar2.png);width:210px;">
								<a  style="text-decoration:none;color:#fff;width:70px;hover:background:none;" href="index.php" onmouseover="this.style.background = 'none'">Home</a>
<span style="margin-left:-30px;display:inline-block;width:30px;color:#fff;">></span>
<a href="#" style="width:80px;text-decoration:underline;color:#fff;margin-left:-30px;" onmouseover="this.style.background = 'none'">Contact Us</a>
							</li>
							
							
							
							
							

						</ul>
				</div>
			</div>	<!--left_content ends-->
			
			
			<div class="center_content" style="width:760px;">
<div style="width:100px;height:auto;float:left;margin-top:50px;margin-bottom:20px;margin-left:-5px;"><img src="images/impact.png" style="margin-left:0px;margin-top:0px;"></div>
	<div style="clear:both;">		
	<p class="tech_inno" style="padding:5px;margin:0px;padding-left:10px;padding-top:0px;text-decoration: underline;clear:both;">Email</p>
			
<p class="tech_inno" style="padding-top:0px;width:700px;">The general enquiries email address for Impact Apps is: <a style="color:#BE721D" href="">info@impactapps.com.au</a></p>
<br>

<p class="tech_inno" style="padding:5px;margin:0px;padding-left:10px;padding-top:20px;text-decoration: underline;">Telephone</p>
			
<p class="tech_inno" style="padding-top:0px;width:700px;">Call Impact Apps on 02 6583 8118 </p>
<p class="tech_inno" style="padding-top:0px;width:700px;">Office hours are Monday to Friday, 9am - 5pm </p>
<br>

<p class="tech_inno" style="padding:5px;margin:0px;padding-left:10px;padding-top:20px;text-decoration: underline;">Post</p>
			
<p class="tech_inno" style="padding-top:0px;width:700px;">The mailing address for Impact Apps is:</p>
<p class="tech_inno" style="padding-top:0px;width:700px;">Impact Apps</p>
<p class="tech_inno" style="padding-top:0px;width:700px;">PO Box 5057</p>
<p class="tech_inno" style="padding-top:0px;width:700px;">Port Macquarie</p>
<p class="tech_inno" style="padding-top:0px;width:700px;">NSW 2444</p>
<br>

<p class="tech_inno" style="padding:5px;margin:0px;padding-left:10px;padding-top:20px;text-decoration: underline;">Submit An Enquiry</p>
			
<p class="tech_inno" style="padding-top:0px;width:700px;">Click on the following link to go to the web page to submit an enquiry.</p><br>
<a href="enquiry.php"><img src="images/submit_online_enquiry_botton.png" style="float:left;margin-top:10px;background:transparent"></a>
<br>
<span class="social_media_span2" style="margin-left:-250px;margin-top:80px;display:inline-block;">
				<!--<a href="https://www.youtube.com" target="_blank"><img src="images/youtube_icon.png" width="36" height="36"></a>-->
				<a href="https://www.twitter.com/WasteInfoApp" target="_blank"><img src="images/twitter_icon.png" width="36" height="36"></a>
				
				<a href="https://www.facebook.com/pages/Waste-Info/160434127454836" target="_blank"><img src="images/facebook_icon.png" width="36" height="36"></a>
				
				
				</span>




</div>




			
			</div>	<!--center_content ends-->
			
			
			
		</div>	<!--container ends-->
			
		</div>	<!--wrapper ends-->
		
			
			<div class="footer_div">
					<ul class="menu_foo">
				<li class="nav"><a  href="index.php" style="color:#1DA442;">Home</a></li>
				<li class="nav"><a href="resident_faq.php">FAQs</a></li>
				<li class="nav"><a href="testimonial.php">Testimonials</a></li>
				<li class="nav" style="width:120px;"><a href="privacy.php" style="width:120px;">Privacy Policy</a></li>
				<li class="nav"><a href="enquiry.php" style="width:120px;">Submit An Enquiry</a></li>
				<span class="nav_foo" style="">Copyright &copy; 2013 Impact Apps</span>
				
			</ul>
		</div>
		
	</body>
	</html>