<?php ob_start();	

require_once('admin/includes/dbconnection.php');


?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

		<title>Waste Info</title>

		<link rel="stylesheet" type="text/css" href="styles/theme.css"/>
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-40855148-1', 'impactapps.com.au');
ga('send', 'pageview');

</script>
<script typ="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script>
function option(value)
{
document.location.href = 'resident_link.php?var=' + value;
}</script>



	</head>
	<body>
		<div class="wrapper">
			
		<div class="header">
			
			<div class="left_header">
			<a href="index.php"><img src="images/logo.png" /></a>
			</div>	<!--left_header ends-->
			
			<div class="right_header">
				<div class="menu_div">
					<ul class="menu">
				<li class="nav"><a  href="index.php" >Home</a></li>
				<li class="nav"><a href="councils.php" >Councils</a></li>
				<li class="nav"><a href="resident.php" style="color:#1DA442;">Residents</a></li>
				<li class="nav" style="width:120px;"><a href="download_app.php" style="width:120px;">Download App</a></li>
				<li class="nav"><a href="about.php">About Us</a></li>
				<li class="nav"><a href="contact.php">Contact Us</a></li>
				
			</ul>
				</div>	<!--menu ends-->
				<div class="header_info">
					<h2 style="">Links To Councils</h2>
					<p></p>
					
					
				</div>	<!--header_info ends-->
				
				
			</div>	<!--right_header ends-->
			
		</div>	<!--header ends-->
		
		<div class="container">
			<div class="left_content">
				<div class="left_menu_div">
					<ul class="left-nav">

							<li class="left-list" style="background: url(images/green_bar2.png);width:210px;">
								<a  style="text-decoration:none;color:#fff;width:70px;hover:background:none;" href="index.php"  onmouseover="this.style.background = 'none'">Home</a>
<span style="margin-left:-30px;display:inline-block;width:30px;color:#fff;">></span>
<a href="resident.php" style="width:70px;text-decoration:underline;color:#fff;margin-left:-30px;" onmouseover="this.style.background = 'none'">Residents</a>
							</li>
							<li class="left-list" >
								<a  href="download_app.php">> Download The App</a>
							</li>
							<li class="left-list" >
								<a  href="resident_video.php">> View Demo Video </a>
							</li>
							<li class="left-list" >
								<a  href="resident_faq.php" >> FAQs</a>
							</li>
							
							<li class="left-list" >
								<a  href="resident_link.php" style="background:#949494;color:#fff">> Links to Councils</a>
							</li>
							
							
							
							

						</ul>
				</div>
			</div>	<!--left_content ends-->
			
			
			<div class="center_content" >
				
				<p class="tech_inno" style="padding:5px;margin:0px;padding-left:10px;padding-top:45px;">
		Select a Council from the drop-down list below:
					<br></p>
				
				
	<div class="	"	>

					<?php
						
					$id =  $_GET['var'];

					$results = mysql_query("SELECT * FROM councilsData WHERE id ='$id'");
					
					while($row = mysql_fetch_array($results))
					
					{
					$name = $row['name'];
					$description = $row['description'];

}
?>
<form action="#" method="post" >
	
<select style="padding:3px;" name="select_coun" onchange="option(this.value)">
<option value="">Choose</option>
<?php 
	$result1= mysql_query("SELECT * FROM councilsData");
				
	while($row2 = mysql_fetch_array($result1))
	{
	if($name == $row2['name']) {
       	$selected = "selected=\"selected\"";
		echo $selected;
		} else {
       	$selected = "";
		}
    	echo "<option value=\"".$row2['id']."\" $selected>".$row2['name']."</option>\n ";					
} ?> 
</select>	
	
	
</form>
</div>
<br>
<div id="rest_info">
<div class="" style="background:none;">
	<?php
$result = mysql_query("SELECT * FROM councilsData WHERE id = '$_GET[var]'");

while($row = mysql_fetch_array($result))

{
$wid=$row['waste_area'];
$name= $row['name'];
$council_logo = $row['logo1'];
$council_site = $row['url'];
}

$result_waste = mysql_query("SELECT * FROM wasteAreas WHERE id = '$wid'");

while($row = mysql_fetch_array($result_waste))

{
$waste_area=$row['waste_area'];
$logo= $row['logo'];
$email= $row['email'];

$description = $row['description'];
$phone = $row['phone'];
$site= $row['url'];

}
?>
<div style="width:700px;">Please note that only councils currently signed up to use the Waste info app will appear in the list</div>
<?php if($_GET['var']!="") { ?>
<a href="http://www.midcoastwaste.com.au" target="_blank"><img src="http://www.impactapps.com.au/files/waste_area_logos/<?php echo $logo; ?>" style="float:left; margin-left:120px; margin-top:20px;" width="250" height="160"></a>
<? } ?>
</div>
<?php if($_GET['var']!="") { ?>
<p class="tech_inno">
	<?php echo $description; ?>
</p>

<p class="tech_inno">Please click on your council logo to find out more information for where you live.</p>



<div class="midcoast2" style="background:none; margin-left:50%;"><a href="<?php echo $council_site; ?>" target="_blank"><img src="http://www.impactapps.com.au/files/council_logos/<?php echo $council_logo; ?>" width="180" height="140"></a></div>
<!--
<div class="midcoast2"><a href="http://www.gtcc.nsw.gov.au" target="_blank"><img src="images/greater_taree_city_council.png" style="margin-top:8px;margin-right:30px;"></a></div>

<div class="midcoast2"><a href="http://www.gloucester.nsw.gov.au" target="_blank"><img src="images/gloucester_shire_concil_logo.png" ></a></div>


</div>
-->
<p class="tech_inno" style="padding-top:50px;">For information about collection services contact :</p>

<p class="tech_inno" style="padding-top:0px; "><?php echo $waste_area; ?></p>

<p class="tech_inno" style="padding-top:0px;">Telephone: 	   <span style="margin-left:10px;display:inline-block;"><?php echo $phone; ?></span></p>
<p class="tech_inno" style="padding-top:0px;">Email:<a style="padding-left:50px;color:blue;" href="mailto:<? echo $site; ?>" ><?php echo $site; ?></a></p>

<p class="tech_inno" style="padding-top:0px;">Website: <a style="padding-left:25px;color:blue;" href="<? echo $email;?> " target="_blank" ><? echo $email; ?> </p>

<? } ?>

			
			</div>	<!--center_content ends-->

			<!--<div class="right_content">
				
				<!--<img style="margin-top:30px;margin-left:55px;" src="images/red_basket.png">-->
			<!--</div>	<!--right_content ends-->
			
			
			
			
		</div>	<!--container ends-->
			
		</div>	<!--wrapper ends-->
		
<?php if($_GET['var']!="") { ?>			
			<div class="footer_div" style=" margin-top:-100px;">
					<ul class="menu_foo" style=" margin-top:-20px;">
				<li class="nav"><a  href="index.php" style="color:#1DA442; margin-top:-40px;">Home</a></li>
				<li class="nav"><a href="resident_faq.php">FAQs</a></li>
				<li class="nav"><a href="testimonial.php">Testimonials</a></li>
				<li class="nav" style="width:120px;"><a href="privacy.php" style="width:120px;">Privacy Policy</a></li>
				<li class="nav"><a href="enquiry.php" style="width:120px;">Submit An Enquiry</a></li>
				<span class="nav_foo" style="margin-top:-1px;">Copyright &copy; 2013 Impact Apps</span>
				
			</ul>
		</div>
<? } ?>	
<?php if($_GET['var']=="") { ?>			
			<div class="footer_div" style="border:1px solid green;">
					<ul class="menu_foo" >
				<li class="nav"><a  href="index.php" style="color:#1DA442;">Home</a></li>
				<li class="nav"><a href="resident_faq.php">FAQs</a></li>
				<li class="nav"><a href="testimonial.php">Testimonials</a></li>
				<li class="nav" style="width:120px;"><a href="privacy.php" style="width:120px;">Privacy Policy</a></li>
				<li class="nav"><a href="enquiry.php" style="width:120px;">Submit An Enquiry</a></li>
				<span class="nav_foo" style="">Copyright &copy; 2013 Impact Apps</span>
				
			</ul>
		</div>
<? } ?>		
	</body>
	</html>