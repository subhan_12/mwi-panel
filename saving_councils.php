<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-40855148-1', 'impactapps.com.au');
ga('send', 'pageview');

</script>
		<title>Waste Info</title>

		<link rel="stylesheet" type="text/css" href="styles/theme.css"/>

	</head>
	<body>
		<div class="wrapper">
			
		<div class="header">
			
			<div class="left_header">
				<a href="index.php"><img src="images/logo.png" /></a>
			</div>	<!--left_header ends-->
			
			<div class="right_header">
				<div class="menu_div">
					<ul class="menu">
				<li class="nav"><a  href="index.php" >Home</a></li>
				<li class="nav"><a href="councils.php" style="color:#1DA442;">Councils</a></li>
				<li class="nav"><a href="resident.php">Residents</a></li>
				<li class="nav" style="width:120px;"><a href="download_app.php" style="width:120px;">Download App</a></li>
				<li class="nav"><a href="about.php">About Us</a></li>
				<li class="nav"><a href="contact.php">Contact Us</a></li>
				
			</ul>
				</div>	<!--menu ends-->
				<div class="header_info">
					<h2 style="">Saving Your Council Money</h2>
					<p></p>
					
					
				</div>	<!--header_info ends-->
				
				
			</div>	<!--right_header ends-->
			
		</div>	<!--header ends-->
		
		<div class="container">
			<div class="left_content">
				<div class="left_menu_div">
					<ul class="left-nav">

						<li class="left-list" style="background: url(images/green_bar2.png);width:210px;">
								<a  style="text-decoration:none;color:#fff;width:70px;hover:background:none;" href="index.php" onmouseover="this.style.background = 'none'">Home</a>
<span style="margin-left:-30px;display:inline-block;width:30px;color:#fff;">></span>
<a href="councils.php" style="width:70px;text-decoration:underline;color:#fff;margin-left:-30px;" onmouseover="this.style.background = 'none'">Councils</a>
							</li>
<li class="left-list" >
								<a  href="technology_innov.php">> Technology Innovation</a>
							</li>
							<li class="left-list" >
								<a  href="#" style="background:#949494;color:#fff">> Saving Your Councils Money</a>
							</li>
							<li class="left-list" >
								<a  href="customized_councils.php"> > Customised To Your Councils </a>
							</li>
							<li class="left-list" >
								<a  href="convenient_residents.php">> Convenient For Residents</a>
							</li>
							
							<li class="left-list" >
								<a  href="report_dumping.php">> Report Littering & Dumping</a>
							</li>
							
							<li class="left-list" >
								<a  href="enquiry.php">> Submit An Enquiry</a>
							</li>
							
							

						</ul>
				</div>
			</div>	<!--left_content ends-->
			
			
			<div class="center_content" >
				
				<p class="tech_inno" style="padding:5px;margin:0px;padding-left:10px;padding-top:45px;">
					Most Councils supply their residents with printed material about collection schedules, waste disposal fees or general advice about what can be recycled. The Waste Info App is more cost-effective at communicating this information as the Council pays a small annual fee and the residents download the App for free. There is potentially significant savings for Council in not printing and distributing materials or taking out expensive newspaper, TV and radio advertisements.
					
					
					
					<br><br>
					
					The costs for the Waste Info App to Council varies according to the population size, as shown in the table below:<br>
					
				</p>
				
				<table class="saving_councils_table" border="0"  width="450" height="200">
				<tr class="tbl_tr" >
					<td class="tbl_head" colspan="4" style="text-align:center;background:#8D8D8D;">Introductory Offer ($ AUD) excl. GST</td>
				</tr>	
				<tr>
					<td class="tbl_head">Council Population</td>
					<td class="tbl_head">Setup Cost</td>
					<td class="tbl_head">Annual Fee</td>
					<td class="tbl_head">Year 1 Cost</td>
				</tr>	
					
					
					<tr class="tbl_tr">
					<td class="tbl_head">Under 10k</td>
					<td class="tbl_head">$2,000</td>
					<td class="tbl_head">$1,500</td>
					<td class="tbl_head">$3,500</td>
				</tr>
				
				<tr class="tbl_tr">
					<td class="tbl_head">10k-20k</td>
					<td class="tbl_head">$2,200</td>
					<td class="tbl_head">$1,800</td>
					<td class="tbl_head">$4,000</td>
				</tr>
				
				<tr class="tbl_tr">
					<td class="tbl_head">20k-30k</td>
					<td class="tbl_head">$2,500</td>
					<td class="tbl_head">$2,200</td>
					<td class="tbl_head">$4,700</td>
				</tr>
				
				<tr class="tbl_tr">
					<td class="tbl_head">30k-100k</td>
					<td class="tbl_head">$2,600</td>
					<td class="tbl_head">$3,000</td>
					<td class="tbl_head">$5,600</td>
				</tr>
				
				
				
				<tr class="tbl_tr">
					<td class="tbl_head">100k-250k</td>
					<td class="tbl_head">$3,300</td>
					<td class="tbl_head">$3,500</td>
					<td class="tbl_head">$6,800</td>
				</tr>
					
					<tr class="tbl_tr">
					<td class="tbl_head">Over 250k</td>
					<td class="tbl_head">$3,800</td>
					<td class="tbl_head">$3,800</td>
					<td class="tbl_head">$7,600</td>
				</tr>
				</table>
				
				<br><br>
				<p class="tech_inno" style="font-weight:bold;">Notes</p>
				
				<ul class="tech_inno_list">
					<li><a class="tech_inno_an">GST needs to be added to above prices</a></li>
					<li><a class="tech_inno_an">Year 1 includes Setup and Annual Fee. In subsequent years, only the Annual Fee is payable.</a></li>
					<li><a class="tech_inno_an">Above prices are for individual councils where the required information can be obtained from a website, contractor or through discussions and where streets and collection services are located on a database.</a></li>
					<li><a class="tech_inno_an">A 50% upfront payment of the Setup Cost is required.</a></li>
					
					
					</ul><br><br>

<p class="tech_inno" style=""><a style="color:blue;" href="contact.php">Contact us</a>&nbsp to discuss what we can do for you.</p>


					
				
			</div>	<!--center_content ends-->
			<div class="right_content">
				<img style="margin-top:40px;margin-left:55px;" src="images/offer_image.png">
				<a href="docs/brochure.pdf"><img style="margin-top:20px;margin-left:65px;" src="images/download_brochure_icon.png"></a>
				<a href="docs/order_form.docx"><img style="margin-top:20px;margin-left:55px;" src="images/download_order_form_botton.png"></a>
				<a href="enquiry.php"><img style="margin-top:20px;margin-left:55px;" src="images/submit_online_enquiry_botton.png"></a>
				<a href="resident_video.php"><img style="margin-top:20px;margin-left:55px;" src="images/video_icon.png"></a>
				
				
			</div>	<!--right_content ends-->
			
			
			
			
		</div>	<!--container ends-->
			
		</div>	<!--wrapper ends-->
		
			
			<div class="footer_div">
					<ul class="menu_foo">
				<li class="nav"><a  href="index.php" style="color:#1DA442;">Home</a></li>
				<li class="nav"><a href="resident_faq.php">FAQs</a></li>
				<li class="nav"><a href="testimonial.php">Testimonials</a></li>
				<li class="nav" style="width:120px;"><a href="privacy.php" style="width:120px;">Privacy Policy</a></li>
				<li class="nav"><a href="enquiry.php" style="width:120px;">Submit An Enquiry</a></li>
				<span class="nav_foo" style="">Copyright &copy; 2013 Impact Apps</span>
				
			</ul>
		</div>
		
	</body>
	</html>