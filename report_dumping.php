<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-40855148-1', 'impactapps.com.au');
ga('send', 'pageview');

</script>
		<title>Waste Info</title>

		<link rel="stylesheet" type="text/css" href="styles/theme.css"/>
<style>
.tech_inno{
	text-align: left;
	}
</style>
	</head>
	<body>
		<div class="wrapper">
			
		<div class="header">
			
			<div class="left_header">
				<a href="index.php"><img src="images/logo.png" /></a>
			</div>	<!--left_header ends-->
			
			<div class="right_header">
				<div class="menu_div">
					<ul class="menu">
				<li class="nav"><a  href="index.php" >Home</a></li>
				<li class="nav"><a href="councils.php" style="color:#1DA442;">Councils</a></li>
				<li class="nav"><a href="resident.php">Residents</a></li>
				<li class="nav" style="width:120px;"><a href="download_app.php" style="width:120px;">Download App</a></li>
				<li class="nav"><a href="about.php">About Us</a></li>
				<li class="nav"><a href="contact.php">Contact Us</a></li>
				
			</ul>
				</div>	<!--menu ends-->
				<div class="header_info">
					<h2 style="">Report Littering & Dumped Rubbish</h2>
					<p></p>
					
					
				</div>	<!--header_info ends-->
				
				
			</div>	<!--right_header ends-->
			
		</div>	<!--header ends-->
		
		<div class="container">
			<div class="left_content">
				<div class="left_menu_div">
					<ul class="left-nav">

							<li class="left-list" style="background: url(images/green_bar2.png);width:210px;">
								<a  style="text-decoration:none;color:#fff;width:70px;hover:background:none;" href="index.php" onmouseover="this.style.background = 'none'">Home</a>
<span style="margin-left:-30px;display:inline-block;width:30px;color:#fff;">></span>
<a href="councils.php" style="width:70px;text-decoration:underline;color:#fff;margin-left:-30px;" onmouseover="this.style.background = 'none'">Councils</a>
							</li>
<li class="left-list" >
								<a  href="technology_innov.php">> Technology Innovation</a>
							</li>
							<li class="left-list" >
								<a  href="saving_councils.php" >> Saving Your Councils Money</a>
							</li>
							<li class="left-list" >
								<a  href="customized_councils.php" >> Customised To Your Councils </a>
							</li>
							<li class="left-list" >
								<a  href="convenient_residents.php">> Convenient For Residents</a>
							</li>
							
							<li class="left-list" >
								<a  href="#" style="background:#949494;color:#fff">> Report Littering & Dumping</a>
							</li>
							
							<li class="left-list" >
								<a  href="enquiry.php">> Submit An Enquiry</a>
							</li>
							
							

						</ul>
				</div>
			</div>	<!--left_content ends-->
			
			
			<div class="center_content" >
				
				<p class="tech_inno" style="padding:5px;margin:0px;padding-left:10px;padding-top:45px;">
					Many Councils spend a large amount of money each year to identify, collect and dispose of litter and dumped rubbish from around their Council area.
					
					
					<br><br>
					
					The Waste Info App is designed to give power to the residents to act as the eyes and ears of the Council in public areas. It then allows Councils to respond more quickly to residents and reduces the need for expensive manual inspections and reporting.
					<br><br>
					They key steps are:
					
				<div class="report_left_info" style="">
					<br>
				<p class="tech_inno" style="word-wrap: break-word;width:300px;"><strong>1.</strong>&nbsp;Resident sees a problem.</p>	
				
				<br><br><br><br><br>
				<p class="tech_inno"  style="word-wrap: break-word;width:300px;"><strong>2.</strong>&nbsp;A photo is taken, with an optional caption.</p>	
				
				<br><br><br><br>
				<p class="tech_inno"  style="word-wrap: break-word;width:300px;"><strong>3.</strong>&nbsp;The phone registers the exact GPS location.</p>
				
				<br><br><br><br>
				<p class="tech_inno" style="word-wrap: break-word;width:300px;"><strong>4.</strong>&nbsp;The ‘SEND’ button is then pressed to email the image and location to the nominated Council email address.</p>
				</div>
				<div class="report_right_info" style="">
					
                    <img src="images/96x96.png">	
					<img src="images/(1)96x96.png">
					<img src="images/(3)96x96.png">
                    <img src="images/(2)96x96.png">
					
					
					
					
				</div>
				<p class="tech_inno" >The report function can also cover the following common problems such as:</p>
				<ul class="tech_inno_list" style="list-style-image:url('images/App-clean-icon.png');">
					<li><a class="tech_inno_an">Illegal dumping</a></li>
					<li><a class="tech_inno_an">A mess left by the waste contractor</a></li>
					<li><a class="tech_inno_an">Littering in public areas</a></li>
					<li><a class="tech_inno_an">Overflowing public litter bin</a></li>
					<li><a class="tech_inno_an">Trees/Branches on roadway</a></li>
					<li><a class="tech_inno_an">Dead animal on roadway</a></li>
					<li><a class="tech_inno_an">Graffiti</a></li>
					<li><a class="tech_inno_an">Other</a></li>
					
					
					
					</ul><br>
				
			</div>	<!--center_content ends-->
			<div class="right_content">
				<img style="margin-top:40px;margin-left:75px;" src="images/iphone_report_a_problem.png">
				
			</div>	<!--right_content ends-->
			
			
			
			
		</div>	<!--container ends-->
			
		</div>	<!--wrapper ends-->
		
			
			<div class="footer_div">
					<ul class="menu_foo">
				<li class="nav"><a  href="index.php" style="color:#1DA442;">Home</a></li>
				<li class="nav"><a href="resident_faq.php">FAQs</a></li>
				<li class="nav"><a href="testimonial.php">Testimonials</a></li>
				<li class="nav" style="width:120px;"><a href="privacy.php" style="width:120px;">Privacy Policy</a></li>
				<li class="nav"><a href="enquiry.php" style="width:120px;">Submit An Enquiry</a></li>
				<span class="nav_foo" style="">Copyright &copy; 2013 Impact Apps</span>
				
			</ul>
		</div>
		
	</body>
	</html>