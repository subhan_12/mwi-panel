<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-40855148-1', 'impactapps.com.au');
ga('send', 'pageview');

</script>
		<title>Waste Info</title>

		<link rel="stylesheet" type="text/css" href="styles/theme.css"/>

	</head>
	<body>
		<div class="wrapper">
			
		<div class="header">
			
			<div class="left_header">
			<a href="index.php"><img src="images/logo.png" /></a>
			</div>	<!--left_header ends-->
			
			<div class="right_header">
				<div class="menu_div">
					<ul class="menu">
				<li class="nav"><a  href="index.php" style="color:#1DA442;">Home</a></li>
				<li class="nav"><a href="councils.php" >Councils</a></li>
				<li class="nav"><a href="resident.php" >Residents</a></li>
				<li class="nav" style="width:120px;"><a href="download_app.php" style="width:120px;">Download App</a></li>
				<li class="nav"><a href="about.php">About Us</a></li>
				<li class="nav"><a href="contact.php">Contact Us</a></li>
				
			</ul>
				</div>	<!--menu ends-->
				<div class="header_info">
					<h2 style="">Privacy Policy</h2>
					<p></p>
					
					
				</div>	<!--header_info ends-->
				
				
			</div>	<!--right_header ends-->
			
		</div>	<!--header ends-->
		
		<div class="container">
			<div class="left_content">
				<div class="left_menu_div">
					<ul class="left-nav">

							<li class="left-list" style="background: url(images/green_bar2.png);width:210px;">
								<a  style="text-decoration:none;color:#fff;width:70px;hover:background:none;" href="index.php" onmouseover="this.style.background = 'none'">Home</a>
<span style="margin-left:-30px;display:inline-block;width:30px;color:#fff;">></span>
<a href="#" style="width:100px;text-decoration:underline;color:#fff;margin-left:-30px;" onmouseover="this.style.background = 'none'">Privacy Policy</a>
							</li>
							
							
							
							
							

						</ul>
				</div>
			</div>	<!--left_content ends-->
			
			
			<div class="center_content" >
				
				<p class="tech_inno" style="padding:5px;margin:0px;padding-left:10px;padding-top:50px;">
			<b style = "text-decoration: underline">Privacy Policy:</b>
					<br><br></p>
				
				
		<p class="tech_inno" style="padding-bottom:0px;"><b>Privacy and Your Rights</b></p>
		<p class="tech_inno" style="padding-top:0px;">Our customers are entitled to expect that we will treat any information provided it by a customer within the terms of relevant privacy responsibilities. Impact Apps has developed a general Privacy Policy which is adhered to by all Business Units and websites within the Impact Apps domain. This policy follows:</p>			
		<br>		
		
		<p class="tech_inno" style="padding-bottom:0px;"><b>Privacy Policy for Visitors and Users of Impact Apps and its Subsites</b></p>
		<p class="tech_inno" style="padding-top:0px;">This Policy applies to all pages on Impact Apps, unless otherwise indicated.</p>			
		<br>		
		
		
		<p class="tech_inno" style="padding-bottom:0px;"><b>Does Impact Apps use cookies?</b></p>
		<p class="tech_inno" style="padding-top:0px;">No. Impact Apps does not use cookies.</p>			
		<br>		
		
		
		<p class="tech_inno" style="padding-bottom:0px;"><b>What information do we collect?</b></p>
		<p class="tech_inno" style="padding-top:0px;">When you look at Impact Apps, we will only collect information that is submitted by a user/customer voluntarily for sales or feedback purposes.</p>			
		<br>
		
		
		
		<p class="tech_inno" style="padding-bottom:0px;"><b>What will you do with the information?</b></p>
		<p class="tech_inno" style="padding-top:0px;">The information collected will only be used for the purpose for which it was collected.</p>			
		<br>
		
		
		
		
		<p class="tech_inno" style="padding-bottom:0px;"><b>What won't you do with the information?</b></p>
		<p class="tech_inno" style="padding-top:0px;">Impact Apps will not disclose or publish any information without consent.</p>			
		<br>
		
		
		
		
		
		<p class="tech_inno" style="padding-bottom:0px;"><b>Who has access to information within Impact Apps?</b></p>
		<p class="tech_inno" style="padding-top:0px;">Impact Apps captures this information on its own computers. Access to the raw data is restricted to a limited number of officers in Impact Apps for the purpose of responding to enquiries or order forms.</p>			
		<br>
		
		
		
		<p class="tech_inno" style="padding-bottom:0px;"><b>Is the raw data stored in a secure way?</b></p>
		<p class="tech_inno" style="padding-top:0px;">Yes. The raw data is stored in a secure format and held by Impact Apps for archival purposes.</p>			
		<br>
		
		
		<p class="tech_inno" style="padding-bottom:0px;"><b>What will you do with information provided in Enquiry Forms?</b></p>
		<p class="tech_inno" style="padding-top:0px;">Enquiry forms are provided by Impact Apps to allow users to ask questions or to comment on the provision of service by Impact Apps. The provision of personal details other than name, location and email address on the enquiry forms is optional.<br>
			Users may provide personal details for the purpose of receiving a reply to their feedback. This information will only be used for the purpose for which it was provided. We will not add your email address or name to any mailing list without consent.
		</p>			
		<br>
		
		<p class="tech_inno" style="padding-bottom:0px;"><b>Will this policy be updated over time?</b></p>
		<p class="tech_inno" style="padding-top:0px;padding-bottom:50px;">Due to the developing nature of privacy principles for online communication, this policy may be modified or expanded in light of new developments or issues that may arise from time to time. Any updates will be published on this web page.
We would be pleased to receive any comments or suggestions on this policy.</p>			
		<br>
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	



			
			</div>	<!--center_content ends-->
			<div class="right_content">
				
				<!--<img style="margin-top:30px;margin-left:55px;" src="images/red_basket.png">-->
			</div>	<!--right_content ends-->
			
			
			
			
		</div>	<!--container ends-->
			
		</div>	<!--wrapper ends-->
		
			
			<div class="footer_div">
					<ul class="menu_foo">
				<li class="nav"><a  href="index.php" >Home</a></li>
				<li class="nav"><a href="resident_faq.php">FAQs</a></li>
				<li class="nav"><a href="testimonial.php">Testimonials</a></li>
				<li class="nav" style="width:120px;"><a href="privacy.php" style="width:120px;color:#1DA442;">Privacy Policy</a></li>
				<li class="nav"><a href="enquiry.php" style="width:120px;">Submit An Enquiry</a></li>
				<span class="nav_foo" style="">Copyright &copy; 2013 Impact Apps</span>
				
			</ul>
		</div>
		
	</body>
	</html>