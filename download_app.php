<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-40855148-1', 'impactapps.com.au');
ga('send', 'pageview');

</script>
		<title>Waste Info</title>

		<link rel="stylesheet" type="text/css" href="styles/theme.css"/>

	</head>
	<body>
		<div class="wrapper">
			
		<div class="header">
			
			<div class="left_header">
			<a href="index.php"><img src="images/logo.png" /></a>
			</div>	<!--left_header ends-->
			
			<div class="right_header">
				<div class="menu_div">
					<ul class="menu">
				<li class="nav"><a  href="index.php" >Home</a></li>
				<li class="nav"><a href="councils.php" >Councils</a></li>
				<li class="nav"><a href="resident.php" style="">Residents</a></li>
				<li class="nav" style="width:120px;"><a href="download_app.php" style="width:120px;color:#1DA442;">Download App</a></li>
				<li class="nav"><a href="about.php">About Us</a></li>
				<li class="nav"><a href="contact.php">Contact Us</a></li>
				
			</ul>
				</div>	<!--menu ends-->
				<div class="header_info">
					<h2>Download The App</h2>
					<p></p>
					
					
				</div>	<!--header_info ends-->
				
				
			</div>	<!--right_header ends-->
			
		</div>	<!--header ends-->
		
		<div class="container">
			<div class="left_content">
				<div class="left_menu_div">
					<ul class="left-nav">

							<li class="left-list" style="background: url(images/green_bar2.png);width:210px;">
	<a  style="text-decoration:none;color:#fff;width:70px;hover:background:none;" href="index.php" onmouseover="this.style.background = 'none'">Home</a>
<span style="margin-left:-30px;display:inline-block;width:30px;color:#fff;">></span>
<a href="resident.php" style="width:70px;text-decoration:underline;color:#fff;margin-left:-30px;" onmouseover="this.style.background = 'none'">Residents</a>
							</li>
							<li class="left-list" >
								<a  href="download_app.php" style="background:#949494;color:#fff">> Download The App</a>
							</li>
							<li class="left-list" >
								<a  href="resident_video.php">> View Demo Video </a>
							</li>
							<li class="left-list" >
								<a  href="resident_faq.php">> FAQs</a>
							</li>
							
							<li class="left-list" >
								<a  href="resident_link.php">> Links to Councils</a>
							</li>
							
							
							
							

						</ul>
				</div>
			</div>	<!--left_content ends-->
			
			
			<div class="center_content" >
				
				<p class="tech_inno" style="padding:5px;margin:0px;padding-left:10px;padding-top:45px;">
This Waste Info App is FREE to all residents.  It has been developed in conjunction with local councils and waste contractors.  It is intended to improve communication from councils to residents and to provide vital waste service information in the palm of your hand.				
					<br>
					
					
					
					
<!--	<ul class="tech_inno_list" style="list-style-image:url('images/App-clean-icon.png');width:460px;">
					<li><a class="tech_inno_an">Set a calendar reminder for when your garbage, recycling or greenwaste services are arriving at your house.</a></li>
					<li><a class="tech_inno_an">See what materials can be recycled and what cannot.</a></li>
					<li><a class="tech_inno_an">Locate where the local waste disposal and recycling and facilities are and how much they cost.</a></li>
					<li><a class="tech_inno_an">Find out what to do with problem waste materials like chemicals, batteries, computers and so on.</a></li>
					<li><a class="tech_inno_an">Report a problem direct to your Council such as a missed pickup, littering or dumped rubbish. You can even take a photo and send that too!</a></li>
					<li><a class="tech_inno_an">Report fallen branches/trees, graffiti, overflowing public bins, dead animals on roadways.</a></li>
					
					
					</ul><br>-->				
					
				
					
					
				</p>
				
<p class="tech_inno" style="">If you are on the smartphone or tablet that you wish to install the App on please go to the Apple or Google Play stores and search for "Waste Info"</p>
				
				<p class="tech_inno" style="">Alternatively click on either the Apple and Google Play stores buttons and this will take you through to the App download page.</p>
				<div class="resident_img" style="margin-bottom:15px;">
					
				<a href="#"><img src="images/app_store_icon.png" style = "margin-left: 25px;"/>	</a>
				<a href="#"><img src="images/google_play_icon.png" /></a>	
					
					
				</div>
				
				
				
		<p class="tech_inno" style="font-weight:bold;margin-left:18px;">HELP THE COUNCIL, HELP THE ENVIRONMENT AND</p>		
		<p class="tech_inno" style="font-weight:bold;margin-left:0px;padding-top:3px;width:550px;">HELP YOURSELF WITH THIS FREE AND INNOVATIVE TOOL!</p>
		
				<br>
				
		<p class="tech_inno" style="margin-left:5px;width:550px;font-weight:bold;"><br>And don’t forget to share this App with family and friends .....</p>		
		
		<div class="social_media">
			
			<span class="social_media_span1">Follow us on: </span>
			
			<span class="social_media_span2">
				<a href="https://www.youtube.com" target="_blank"><img src="images/youtube_icon.png" width="36" height="36"></a>
				<a href="https://www.twitter.com/WasteInfoApp" target="_blank"><img src="images/twitter_icon.png" width="36" height="36"></a>
				
				<a href="https://www.facebook.com/pages/Waste-Info/160434127454836" target="_blank"><img src="images/facebook_icon.png" width="36" height="36"></a>
				
				
				</span>
			
		</div>
		
	
		
				
				
				
				
				
				
				
				
			</div>	<!--center_content ends-->
			<div class="right_content">
				
				<img style="margin-top:40px;margin-left:55px;" src="images/iphone_collection_schedule.png">
			</div>	<!--right_content ends-->
			
			
			
			
		</div>	<!--container ends-->
			
		</div>	<!--wrapper ends-->
		
			
			<div class="footer_div">
					<ul class="menu_foo">
				<li class="nav"><a  href="index.php" style="color:#1DA442;">Home</a></li>
				<li class="nav"><a href="resident_faq.php">FAQs</a></li>
				<li class="nav"><a href="testimonial.php">Testimonials</a></li>
				<li class="nav" style="width:120px;"><a href="privacy.php" style="width:120px;">Privacy Policy</a></li>
				<li class="nav"><a href="enquiry.php" style="width:120px;">Submit An Enquiry</a></li>
				<span class="nav_foo" style="">Copyright &copy; 2013 Impact Apps</span>
				
			</ul>
		</div>
		
	</body>
	</html>