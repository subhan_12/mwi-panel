<?php ob_start();	

require_once('includes/dbconnection.php');

session_start();
if(!isset($_SESSION['user_id']))
{
header('location:index.php');


}
$id =  $_GET['id'];
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>My Waste Info</title>
		<link rel="stylesheet" type="text/css" href="styles/index.css"/>

	</head>

<script>

function validateForm(){	
var title = document.getElementById('select_tag').value;
var waste = document.getElementById('waste').value;
var image = document.getElementById('file').value;
var image1 = document.getElementById('files').value;

if(title == "null" && waste == "") { 

alert("Enter the Required Fields.");
return false;
}

if(waste == "" && image == "") { 

alert("Enter the Required Fields.");
return false;
}


if(title=="null"){
	alert("Enter the State.");
	return false;
	}
	
if(waste==""){
	alert("Enter the Waste Area.")
	return false;
	}

}


</script>


	<body>
		
		<div class="logo_div">
		<div class="az_log">
			<img src="images/Waste-info-150-icon (1).png " width="75" height="75">
			
			<div style = "width: 300px; height: 100px; border: 0px solid red; margin-left: 400px; margin-top: -65px; font-size: 50px; font-weight: bold; color: #fff;">Waste Area</div>
			
			<div class="dash_logout">
				
				<span style = ""><img src="images/Apps-session-logout-icon.png">&nbsp;<a href="php_scripts/logout.php">Logout</a></span>
			<br>
			<span style="margin-top:5px;display:inline-block;"><img src="images/Administrator-icon.png">&nbsp;Hello Admin!</span>
			</div>
			
		</div>	
			
			
		</div>	<!-- logo_div ends>-->
		<div class="wrapper">
			
		<div class="menu_div">
			
			<ul class="menu">
				<li class="nav" ><a  href="dashboard.php">Dashboard</a></li>
				<li class="nav" ><a  href="states.php">States</a></li>
				<li class="nav" style="background: url(images/green_bar2.png);"><a style="color:#fff" href="waste.php">Waste Area</a></li>
				<li class="nav"><a href="councils.php">Councils</a></li>
				<li class="nav"><a href="towns.php">Towns</a></li>
				<li class="nav"><a href="streets.php">Streets</a></li>
				<li class="nav" style="width:148px;hover:width:148px;"><a style="width:148px;" href="settings.php">Settings</a></li>
				
				
				
			</ul>
			
			
		</div>	<!-- menu_div ends>-->
			
		<div class="az-container">
				<div class="az-left-content">

					<div class="az-left_menu">

						<ul class="left-nav">

							<li class="left-list" style="background: url(images/green_bar2.png);">
								<a  style="color:#fff">Information</a>
							</li>
							
							<li class="left-list" >
								<a>Collection Details</a>
							</li>
							
							<li class="left-list" >
								<a>Materials</a>
							</li>
							<li class="left-list" >
								<a>FAQs and News</a>
							</li>

						</ul>

					</div>
					<!--az-left_menu-->

				</div>
				<!--az-left-content-->

				<div class="az-right-content">
					
					<center><h2 class="wa_info_head">Add a Waste Area</h2></center>
					
					
				<div class="wa_info_formarea">
				<?php

				$result = mysql_query("SELECT * FROM wasteAreas WHERE id = '$_GET[id]'");
				
				while($row = mysql_fetch_array($result))
				
				{
				    
				$state = $row['state'];
				
				$waste_area = $row['waste_area'];
				$image = $row['logo'];
				$image2 = $row['logo2'];
				$email =$row['email'];
				$phone = $row['phone'];
				$url = $row['url'];
				$description =$row['description'];
				
				 } ?>

<form name="login_form" class="az-login" method="POST" action="php_scripts/info_script.php?id=<?php echo $_GET['id']; ?>" enctype="multipart/form-data" onsubmit="return validateForm()">
					<label>State:</label>
						<select class="wa_info_select" name="state" id="select_tag">
							  <option value="null" selected="selected">Select</option>
							  <?php 
							  $result1= mysql_query("SELECT * FROM states");
				
								while($row2 = mysql_fetch_array($result1))
				
									{
    								if($state == $row2['id']) {
        							$selected = "selected=\"selected\"";
									echo $selected;
									} else {
       								$selected = "";
									}
    								echo "<option value=\"".$row2['id']."\" $selected>".$row2['state']."</option>\n ";
	
									
				 } ?> 
							  

						</select>
						<br>
						<label>Waste Area:</label>
						<input type="text" id="waste" name="waste" class="wa_info_text" value="<?php echo $waste_area; ?>" />
						<br>
						<div style="width:150px; float:left; margin-left:-40px;color:#fff;">Website Contact no:</div>
						<input type="text" id="waste" name="phone" class="wa_info_text" value="<?php echo $phone; ?>" />
						<br>
						<div style="width:110px; float:left; margin-left:0px;color:#fff;">Website Email:</div>
						<input type="text" id="waste" name="url" class="wa_info_text" value="<?php echo $url; ?>" />
						<br>
						<div style="width:115px; float:left; margin-left:-5px;color:#fff;">Website URL:</div>
						<input type="text" id="waste" name="email" class="wa_info_text" value="<?php echo $email; ?>" />
						<br>
						<div style="width:150px; float:left; margin-left:-40px;color:#fff;">Website Description:</div>
						<textarea name="description" rows="7" cols="10" class="wa_info_text" style="width:260px; height:100px;resize:none;" /><?php echo $description; ?></textarea>
						<br>
						<label>Logo:</label>
						
						<input type="file" id="file" name="file" class="wa_info_text" style="width:85px;" value="" /><br>
<span style="font-style:italic; color:red; font-size:11px;display:inline-block;margin-left:113px;">Size: 421 x 217</span>

						<br>
						<label>Logo 2x:</label>
						<input type="file" id="files" name="file1" class="wa_info_text" style="width:85px;" value="" /><br>
<span style="font-style:italic; color:red; font-size:11px;display:inline-block;margin-left:113px;">Size: 842 x 434</span>

					
						<?php if($image2) { ?>
						<img src="http://www.impactapps.com.au/files/waste_area_logos/<?php echo $image2; ?>" height="100" width="100" style="margin-right:10px; margin-top:-80px; float:right; " /><? } else { ?>
						<img src="images/dummy.png" height="100" width="100" style="margin-right:10px; margin-top:-80px; float:right;  " /><? } ?>
						<?php if($image) { ?>
						<img src="http://www.impactapps.com.au/files/waste_area_logos/<?php echo $image; ?>" height="100" width="100" style="margin-right:10px; margin-top:-80px; float:right; " /><? } else { ?>
						<img src="images/dummy.png" height="100" width="100" style="margin-right:10px; margin-top:-80px; float:right; " /><? } ?>
						
						<br>
						
						<input type="submit" name="submit" class="az-submit  az-btn" value="Save and Next" style="width:110px; margin-left:113px;"> 
<a href="#" class="az-btn" style="width:40px; margin-left:0px;">Cancel</a>
						<?php 
						if($_GET['a']) { ?> <span style="color:white; margin-left:10px;">Logo is Invalid</span><? }	
						if($_GET['b']) { ?> <span style="color:white; margin-left:10px;">Logo 2x is Invalid</span><? } ?>
					</form>
					
					
				</div>	<!--wa_info_formarea-->
					
					
					
				</div>
				<!--az-right-content ends-->

			</div>
			<!--az-container ends-->
			
		</div>	<!--wrapper ends>-->
		<div class="az-copyright">
			<p>
				Copyright &copy; MyWasteInfo
			</p>
		</div>
	</body>
</html>
