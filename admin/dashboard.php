<?php ob_start();	

require_once('includes/dbconnection.php');

session_start();
if(!isset($_SESSION['user_id']))
{
header('location:index.php');
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>My Waste Info</title>

		<link rel="stylesheet" type="text/css" href="styles/index.css"/>

	</head>
	<body>
		
		<div class="logo_div">
		<div class="az_log">
			<img src="images/Waste-info-150-icon (1).png " width="75" height="75">
			
			<div style = "width: 300px; height: 100px; border: 0px solid red; margin-left: 350px; margin-top: -65px; font-size: 50px; font-weight: bold; color: #fff;">DashBoard</div>
			
			<div class="dash_logout">
				
				<span style = ""><img src="images/Apps-session-logout-icon.png">&nbsp;<a href="php_scripts/logout.php">Logout</a></span>
			<br>
			<span style="margin-top:5px;display:inline-block;"><img src="images/Administrator-icon.png">&nbsp;Hello Admin!</span>
			</div>
			
		</div>	
			
			
		</div>	<!-- logo_div ends>-->
		<div class="wrapper">
			
		<div class="menu_div">
			
			<ul class="menu">
				<li class="nav" style="background: url(images/green_bar2.png);"><a style="color:#fff" href="dashboard.php">Dashboard</a></li>
				<li class="nav"><a href="states.php">States</a></li>
				<li class="nav"><a href="waste.php">Waste Area</a></li>
				<li class="nav"><a href="councils.php">Councils</a></li>
				<li class="nav"><a href="towns.php">Towns</a></li>
				<li class="nav"><a href="streets.php">Streets</a></li>
				<li class="nav" style="width:148px;hover:width:148px;"><a style="width:148px;" href="settings.php">Settings</a></li>
				
				
				
			</ul>
			
			
		</div>	<!-- menu_div ends>-->
			
		<div class="az-container">
			
            
            <?
            $date = date("Y-m-d G:i:s");
            $result = mysql_query("SELECT * FROM admin");
			while($row = mysql_fetch_array($result))
			{
            	$ll = $row['last_login'];
           	}
			
			
			
			mysql_query("UPDATE admin set last_login = '$date' WHERE id = '1'");
			
			
			
            ?>
            
            
            <?
            
			
			$resultst = mysql_query("SELECT * FROM states");
			$countst = mysql_num_rows($resultst);
			
			
			?>
            
            
            <?
            
			
			$resultw = mysql_query("SELECT * FROM wasteAreas");
			$countw = mysql_num_rows($resultw);
			
			
			?>
            
            
            <?
            
			
			$resultci = mysql_query("SELECT * FROM councilsData WHERE council_type = 'Independent'");
			$countci = mysql_num_rows($resultci);
			
			
			?>
            
             <?
            
			
			$resultcd = mysql_query("SELECT * FROM councilsData WHERE council_type = 'Dependent'");
			$countcd = mysql_num_rows($resultcd);
			
			
			?>
            
            <?
            
			
			$resultt = mysql_query("SELECT DISTINCT town FROM townData");
			$countt= mysql_num_rows($resultt);
			
			
			?>
            
             <?
            
			
			$resultstr = mysql_query("SELECT * FROM schedule");
			$countstr= mysql_num_rows($resultstr);
			
			
			?>
            
            
            
		<div class="az-dashboard-info">
			<div class="dash_span_wrapper">
			<span class="dash_q_info">Last Login:</span>	<span class="dash_a_info"><? echo date(  "F j, Y, g:i a", strtotime( $ll) ); ?></span>
			<span class="dash_q_info">Total States:</span>	<span class="dash_a_info"><? echo $countst; ?></span>
			<span class="dash_q_info">Waste Area:</span>	<span class="dash_a_info"><? echo $countw; ?></span>
			<span class="dash_q_info">Independent Councils: </span>	<span class="dash_a_info"><? echo $countci; ?></span>
			<span class="dash_q_info">Dependent Councils:   </span>	<span class="dash_a_info"><? echo $countcd; ?></span>
			<span class="dash_q_info">Total Towns:</span>	<span class="dash_a_info"><? echo $countt; ?></span>
			<span class="dash_q_info">Total Streets:</span>	<span class="dash_a_info"><? echo $countstr; ?></span>
			
			
			
			</div>
		</div>	<!--az-dashboard-info-->
			
			
			
		</div>	<!--az-container ends-->
			
		</div>	<!--wrapper ends>-->
		<div class="az-copyright">
			<p>
				Copyright &copy; MyWasteInfo
			</p>
		</div>
			
	</body>
</html>
