<?php ob_start();	

require_once('includes/dbconnection.php');

$id =  $_GET['id'];
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<script type="text/javascript" src="Tiny/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
	tinyMCE.init({
		// General options
		mode : "textareas",
		theme : "advanced",
		skin : "o2k7",
        skin_variant : "silver",
		height: "640",
		width: "910",
		
		plugins : "jbimages,autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups,autosave",

		// Theme options
		language : "en",
		theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
		theme_advanced_buttons4 : "jbimages,|,insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,
		theme_advanced_background_colors : "FF00FF,FFFF00,000000",

		// Example word content CSS (should be your site CSS) this one removes paragraph margins
		content_css : "css/word.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",

		// Replace values for the template plugin
		template_replace_values : {
			username : "Some User",
			staffid : "991234",
		relative_urls : false
		}
	});
		
</script>


	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>My Waste Info</title>
		<link rel="stylesheet" type="text/css" href="styles/index.css"/>

	</head>




	<body>
		
		<div class="logo_div">
		<div class="az_log">
			<img src="images/Waste-info-150-icon (1).png " width="75" height="75">
			
			<div style = "width: 300px; height: 100px; border: 0px solid red; margin-left: 400px; margin-top: -65px; font-size: 50px; font-weight: bold; color: #fff;">Council</div>
			
			<div class="dash_logout">
				
				<span style = ""><img src="images/Apps-session-logout-icon.png">&nbsp;<a href="php_scripts/logout.php">Logout</a></span>
			<br>
			<span style="margin-top:5px;display:inline-block;"><img src="images/Administrator-icon.png">&nbsp;Hello Admin!</span>
			</div>
			
		</div>	
			
			
		</div>	<!-- logo_div ends>-->
		<div class="wrapper">
			
		<div class="menu_div">
			
			<ul class="menu">
				<li class="nav" ><a  href="dashboard.php">Dashboard</a></li>
				<li class="nav" ><a  href="states.php">States</a></li>
				<li class="nav" ><a  href="waste.php">Waste Area</a></li>
				<li class="nav" style="background: url(images/green_bar2.png);"><a style="color:#fff" href="councils.php">Councils</a></li>
				<li class="nav"><a href="towns.php">Towns</a></li>
				<li class="nav"><a href="streets.php">Streets</a></li>
				<li class="nav" style="width:148px;hover:width:148px;"><a style="width:148px;" href="settings.php">Settings</a></li>	
			</ul>
			
			
		</div>	<!-- menu_div ends>-->
			
		<div class="az-container" style="">
			<a href="council_info.php" class="az-btn" style="margin:50px; margin-right:0;margin-left:60px;width:90px;">Add a Council</a>
			<a href="coun.php" class="az-btn" style="margin-left:10px;width:70px;">Council Info</a>
<div class="council_area">

<form name="coun_form" action="php_scripts/coun_script.php" method="post">

<div style="margin-left:20px; margin-top:20px; float:left;">All Councils:</div>
<div style="margin-left:120px; padding-top:15px; ">
<select style="padding:3px;" name="select_coun">
<option value="">Choose</option>
<?php

$result = mysql_query("SELECT * FROM councilsData");

while($row = mysql_fetch_array($result))

{

echo "<option value='$row[id]'>$row[name]</option>";
}


?>
</select>
</div>
</br>

<div style="margin-left:20px;">
				 <label>Content:</label><br><br>
 				<textarea id="elm1" name="elm1" rows="15" cols="100" style="width:90%; margin-left:10px;" onfocus="pass()" ></textarea>				
					</div>
				
				<div style="margin-left:25px;">
				<input type="submit" name="submit" class="az-submit  az-btn" value="Save" style="width:60px;margin-left:-3px">
</form>
				</div>

</div>
			
		</div>	<!--az-container ends-->
			
		</div>	<!--wrapper ends>-->
		<div class="az-copyright">
			<p>
				Copyright &copy; MyWasteInfo
			</p>
		</div>
	</body>
</html>
