<?php ob_start();	

require_once('includes/dbconnection.php');

session_start();
if(!isset($_SESSION['user_id']))
{
header('location:index.php');


}
$id =  $_GET['id'];
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>My Waste Info</title>
		<link rel="stylesheet" type="text/css" href="styles/index.css"/>
<link href="calendar.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="cal.js"></script>
<script type="text/javascript">
jQuery(document).ready(function () {
	
	//$('input.three').simpleDatepicker();
	$('input.one').simpleDatepicker({ startdate: 1900, enddate: 2025, x: 45, y: 10});
	$('input.two').simpleDatepicker({ chosendate: '9/9/2010', startdate: '6/10/2008', enddate: '7/20/2011', x: 0, y: 25});
	$('input.three').simpleDatepicker({ x: 45, y: 3 });
});
</script>

	</head>



	<body>
		
		<div class="logo_div">
		<div class="az_log">
			<img src="images/Waste-info-150-icon (1).png " width="75" height="75">
			
			<div style = "width: 300px; height: 100px; border: 0px solid red; margin-left: 420px; margin-top: -65px; font-size: 50px; font-weight: bold; color: #fff;">Towns</div>
			
			<div class="dash_logout">
				
				<span style = ""><img src="images/Apps-session-logout-icon.png">&nbsp;<a href="php_scripts/logout.php">Logout</a></span>
			<br>
			<span style="margin-top:5px;display:inline-block;"><img src="images/Administrator-icon.png">&nbsp;Hello Admin!</span>
			</div>
			
		</div>	
			
			
		</div>	<!-- logo_div ends>-->
		<div class="wrapper">
			
		<div class="menu_div">
			
			<ul class="menu">
				<li class="nav" ><a  href="dashboard.php">Dashboard</a></li>
				<li class="nav" ><a  href="states.php">States</a></li>
				<li class="nav"><a href="waste.php">Waste Area</a></li>
				<li class="nav"><a href="councils.php">Councils</a></li>
				<li class="nav" ><a   href="towns.php" >Towns</a></li>
				<li class="nav" style="background: url(images/green_bar2.png);"><a  style="color:#fff" href="streets.php">Streets</a></li>
				<li class="nav" style="width:148px;hover:width:148px;"><a style="width:148px;" href="settings.php">Settings</a></li>
				
				
				
			</ul>
			
			
		</div>	<!-- menu_div ends>-->
			
		<div class="az-container" style="">
        
        <div style="width:1000px;min-height:100px;margin-left:0;margin-top:30px;padding:0px;">
<?php

				$result = mysql_query("SELECT * FROM schedule WHERE id = '$_GET[id]'");
				
				while($row = mysql_fetch_array($result))
				
				{
				    
				$town = $row['suburb'];
				
				$recycling = $row['recycling'];
				$garbage = $row['garbage'];
				$organic = $row['organic'];
				$street = $row['street'];

				
				 } ?>
        <form name="login_form" class="az-login" id="login" action="php_scripts/tw_script.php?id=<?php echo $_GET['id']; ?>" method="POST" onsubmit="return validateForm()" style="margin:0px;margin-left:0px;">

						<span style="margin-left:10px; color:#fff;">Towns</span>
						<select class="council_select" style="width:170px;" name="tid">
                        <option value="null">Select</option>
                        <?php 
						$result1= mysql_query("SELECT id, town FROM townData ORDER BY town ASC");
							while($row1 = mysql_fetch_array($result1))
								{
    								if($town == $row1['town']) {
        							$selected = "selected=\"selected\"";
									echo $selected;
									} else {
       								$selected = "";
									}
    								echo "<option value=\"".$row1['id']."\" $selected>".$row1['town']."</option>\n ";
				 		} ?> 
                          </select>

						<span style="margin-left:10px; color:#fff;">Recycling</span>
						<input type="text" name="recycling" width="200" class="one" value="<?php echo $recycling;?>" />

						<span style="margin-left:10px; color:#fff;">Garbage</span>
						<input type="text" name="garbage" width="200" class="two" value="<?php echo $garbage;?>" />

						<span style="margin-left:10px; color:#fff;">Organics</span>
						<input type="text" name="organics" width="200" class="three" value="<?php echo $organic;?>" />
						<!--
						<select class="council_select" style="width:170px;" name="day">
						
						<option value="null">Select</option>
						
						<? 
							

						switch ($day)
						{
						case "Monday":
						
						echo "<option value='Monday' selected>Monday</option>";
						echo "<option value='Tuesday'>Tuesday</option>";
						echo "<option value='Wednesday'>Wednesday</option>";
						echo "<option value='Thursday'>Thursday</option>";
						echo "<option value='Friday'>Friday</option>";
						echo "<option value='Saturday'>Saturday</option>";
						echo "<option value='Sunday'>Sunday</option>";
						break;

						case "Tuesday":
						
						echo "<option value='Monday'>Monday</option>";
						echo "<option value='Tuesday' selected>Tuesday</option>";
						echo "<option value='Wednesday'>Wednesday</option>";
						echo "<option value='Thursday'>Thursday</option>";
						echo "<option value='Friday'>Friday</option>";
						echo "<option value='Saturday'>Saturday</option>";
						echo "<option value='Sunday'>Sunday</option>";
						break;
					
						case "Wednesday":
						
						echo "<option value='Monday'>Monday</option>";
						echo "<option value='Tuesday'>Tuesday</option>";
						echo "<option value='Wednesday' selected>Wednesday</option>";
						echo "<option value='Thursday'>Thursday</option>";
						echo "<option value='Friday'>Friday</option>";
						echo "<option value='Saturday'>Saturday</option>";
						echo "<option value='Sunday'>Sunday</option>";
						break;

						case "Thursday":
						
						echo "<option value='Monday'>Monday</option>";
						echo "<option value='Tuesday'>Tuesday</option>";
						echo "<option value='Wednesday'>Wednesday</option>";
						echo "<option value='Thursday' selected>Thursday</option>";
						echo "<option value='Friday'>Friday</option>";
						echo "<option value='Saturday'>Saturday</option>";
						echo "<option value='Sunday'>Sunday</option>";
						break;

						case "Friday":
						
						echo "<option value='Monday'>Monday</option>";
						echo "<option value='Tuesday'>Tuesday</option>";
						echo "<option value='Wednesday'>Wednesday</option>";
						echo "<option value='Thursday'>Thursday</option>";
						echo "<option value='Friday' selected>Friday</option>";
						echo "<option value='Saturday'>Saturday</option>";
						echo "<option value='Sunday'>Sunday</option>";
						break;
						
						case "Saturday":
						 
						echo "<option value='Monday'>Monday</option>";
						echo "<option value='Tuesday'>Tuesday</option>";
						echo "<option value='Wednesday'>Wednesday</option>";
						echo "<option value='Thursday'>Thursday</option>";
						echo "<option value='Friday'>Friday</option>";
						echo "<option value='Saturday' selected>Saturday</option>";
						echo "<option value='Sunday'>Sunday</option>";
						break;
				
						case "Sunday":
						
						echo "<option value='Monday'>Monday</option>";
						echo "<option value='Tuesday'>Tuesday</option>";
						echo "<option value='Wednesday'>Wednesday</option>";
						echo "<option value='Thursday'>Thursday</option>";
						echo "<option value='Friday'>Friday</option>";
						echo "<option value='Saturday'>Saturday</option>";
						echo "<option value='Sunday' selected>Sunday</option>";
						break;
						
						default:
						echo "<option value='Monday'>Monday</option>";
						echo "<option value='Tuesday'>Tuesday</option>";
						echo "<option value='Wednesday'>Wednesday</option>";
						echo "<option value='Thursday'>Thursday</option>";
						echo "<option value='Friday'>Friday</option>";
						echo "<option value='Saturday'>Saturday</option>";
						echo "<option value='Sunday'>Sunday</option>";
						}
						
						?> 
							
                          </select>-->
  <!--
						<label>Time</label>
						<select class="council_select" style="width:170px;" name="time">
                        <option value="null">Select</option>
						<?php $result4= mysql_query("SELECT * FROM timeData");
							while($row4 = mysql_fetch_array($result4))
								{
    								if($time == $row4['time']) {
        							$selected = "selected=\"selected\"";
									echo $selected;
									} else {
       								$selected = "";
									}
    								echo "<option value=\"".$row4['time']."\" $selected>".$row4['time']."</option>\n ";
				 		} ?>


                          </select>
                          <!-- <a href="#" class="az-btn" style="width:120px;margin-left:10px;">Import from CSV</a>-->
						
						<br>
						<label>Street Name</label>
						<input type="text" name="street" class="az-user" onfocus="p()" name="street" style="border:1px solid black;margin-left:5px;" value="<? echo $street; ?>" />
						<br>
						<label></label>
						<input type="submit" name="submit" class="az-submit  az-btn" value="Submit" style="width:100px;margin-left:3px;">
						

					</form>
        
        
        </div>
			
<div style="width:900px; height:899px; margin-top:50px; margin-left:60px;">




<table width="905" border="0" height="30" cellspacing="0">
  <tr>
    
    <th bgcolor="#E4E4E4" align="left" style="width:200px; padding-left:5px; border-bottom:solid 1px  #CCC;">Recycling</td>
    <th bgcolor="#E4E4E4" align="left" style="width:200px;border-bottom:solid 1px  #CCC;">Organics</td>
    <th bgcolor="#E4E4E4" align="left" style="width:300px;border-bottom:solid 1px  #CCC;">Street</td>
    <th bgcolor="#E4E4E4" align="center" style="width:80px;border-bottom:solid 1px  #CCC;"></td>
     <th bgcolor="#E4E4E4" align="center" style="width:80px;border-bottom:solid 1px  #CCC;"></td>
  </tr>
</table> 

  <div id="table_div_browse"><!-- Table Div starts -->


<form name="login_form" action="" method="post">
<table width="905" border="0" cellspacing="0">
<?php

$id = $_GET[id];
$resul5 = mysql_query("SELECT * FROM schedule");

while($row5 = mysql_fetch_array($resul5))

{ ?>
  <tr>
    <td bgcolor="#F4F4F4" align="left" style="width:200px; padding-left:5px; border-bottom:solid 1px  #CCC;"><? echo $row5['recycling']; ?></td>
    <td bgcolor="#F4F4F4" align="left" style="width:200px; border-bottom:solid 1px  #CCC;"><? echo $row5['organic']; ?></td>
    <td bgcolor="#F4F4F4" align="left" style="width:300px; border-bottom:solid 1px  #CCC;"><? echo $row5['street']; ?></td>
    <td bgcolor="#F4F4F4" align="center" style="width:80px;border-bottom:solid 1px  #CCC;"><img src="images/edit_icon.png"/><a style="text-decoration:none; color:#000;" href="streets.php?id=<? echo $row5['id']; ?>">edit</a></td>
    <td bgcolor="#F4F4F4" align="center" style="width:80px;border-bottom:solid 1px  #CCC; padding-right:15px;"><img src="images/delete_icon.png"/><a style="text-decoration:none; color:#000;" onclick="javascript:return confirm('Are you sure you want to delete ?')" href="php_scripts/delete_street.php?id=<? echo $row5['id']; ?>"> delete</a></td>
  </tr>

<?php } ?>
</table>

</form>
  
    </div><!-- Table Div Ends -->

</div>
		
			
			
			
		</div>	<!--az-container ends-->
			
		</div>	<!--wrapper ends>-->
		<div class="az-copyright">
			<p>
				Copyright &copy; MyWasteInfo
			</p>
		</div>
	</body>
</html>
