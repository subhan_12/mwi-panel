<?php ob_start();	

require_once('includes/dbconnection.php');

session_start();
if(!isset($_SESSION['user_id']))
{
header('location:index.php');
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>My Waste Info</title>
		<link rel="stylesheet" type="text/css" href="styles/index.css"/>

	</head>


<script type="text/javascript">
function validateForm(){	
var id = document.forms["state_form"]["state"].value;

if(id == ""){
document.forms["state_form"]["state"].style.border = "1px solid red";
document.forms["state_form"]["state"].style.background = "#F9AA9E url('images/error.png') 240px center no-repeat";
return false;
}
	
}

function pass(){
document.forms["state_form"]["state"].style.border = "1px solid #635843";
document.forms["state_form"]["state"].style.background = "white";
}


</script>

	<body>
		
		<div class="logo_div">
		<div class="az_log">
			<img src="images/Waste-info-150-icon (1).png " width="75" height="75">
			
			<div style = "width: 300px; height: 100px; border: 0px solid red; margin-left: 420px; margin-top: -65px; font-size: 50px; font-weight: bold; color: #fff;">States</div>
			
			<div class="dash_logout">
				
				<span style = ""><img src="images/Apps-session-logout-icon.png">&nbsp;<a href="php_scripts/logout.php">Logout</a></span>
			<br>
			<span style="margin-top:5px;display:inline-block;"><img src="images/Administrator-icon.png">&nbsp;Hello Admin!</span>
			</div>
			
		</div>	
			
			
		</div>	<!-- logo_div ends>-->
		<div class="wrapper">
			
		<div class="menu_div">
			
			<ul class="menu">
				<li class="nav" ><a  href="dashboard.php">Dashboard</a></li>
				<li class="nav" style="background: url(images/green_bar2.png);"><a style="color:#fff" href="states.php">States</a></li>
				<li class="nav"><a href="waste.php">Waste Area</a></li>
				<li class="nav"><a href="councils.php">Councils</a></li>
				<li class="nav"><a href="towns.php">Towns</a></li>
				<li class="nav"><a href="streets.php">Streets</a></li>
				<li class="nav" style="width:148px;hover:width:148px;"><a style="width:148px;" href="settings.php">Settings</a></li>
				
				
				
			</ul>
			
			
		</div>	<!-- menu_div ends>-->
			
		<div class="az-container" style="">
			
<div style="width:900px; height:899px; margin-top:50px; margin-left:60px;">

<?php

$id = $_GET[id];
$result = mysql_query("SELECT * FROM states WHERE id = '$_GET[id]'");

while($row = mysql_fetch_array($result))

{
    
$state = $row['state'];


 } ?>


<form action="php_scripts/state_script.php?id=<?php echo $id;?>" method="post" name="state_form" onsubmit="return validateForm()">

    <div style="margin:auto; width:900px;"><label style="float:left;padding-right:10px; margin-top:5px;color:#fff">Enter State:</label>
    <input type="text" id="title" name="state" value="<?php echo $state; ?>" style="padding:5px; width:250px; height:15px; border:1px solid black;" maxlength="50" onfocus="pass()" />
    <input type="submit" value="Save" class="az-btn" style="margin-left:10px;width:90px;" >
     <input type="reset" value="Clear" class="az-btn" style="margin-left:0px;width:80px;" >
    </div><br />


</form>

<table width="905" border="0" cellspacing="0">
  <tr>
    
    <th bgcolor="#E4E4E4" align="left" style="width:720px; border-bottom:solid 1px  #CCC;padding-left:10px;">States</td>
    <th bgcolor="#E4E4E4" align="right" style="border-bottom:solid 1px  #CCC;padding-right:10px;">Actions</td>
    <th bgcolor="#E4E4E4" align="center" style="border-bottom:solid 1px  #CCC;"></td>
  </tr>
</table> 

  <div id="table_div_browse"><!-- Table Div starts -->

<?php

$select = "SELECT * FROM states ORDER BY ID DESC";
$result = mysql_query($select);

while($row = mysql_fetch_array($result)){
    
?>
<form name="login_form" action="states.php?id=<?php echo $row['id']; ?>" method="post">
<table width="905" border="0" cellspacing="0">
  <tr>
    <td bgcolor="#F4F4F4" align="left" style="width:720px; border-bottom:solid 1px  #CCC;padding-left:10px;"><?php echo $row['state']; ?>  </td>
    <td bgcolor="#F4F4F4" align="center" style="border-bottom:solid 1px  #CCC;"><img src="images/edit_icon.png"/><a style="text-decoration:none; color:#000;" href="states.php?id=<?php echo $row['id']; ?>">edit</a></td>
    <td bgcolor="#F4F4F4" align="center" style="border-bottom:solid 1px  #CCC;"><img src="images/delete_icon.png"/><a style="text-decoration:none; color:#000;" onclick="javascript:return confirm('Are you sure you want to delete ?')" href="php_scripts/delete_states.php?id=<?php echo $row['id']; ?>"> delete</a></td>
  </tr>

</table>
 <?php } ?>
</form>
  
    </div><!-- Table Div Ends -->

</div>
		
			
			
			
		</div>	<!--az-container ends-->
			
		</div>	<!--wrapper ends>-->
		<div class="az-copyright">
			<p>
				Copyright &copy; MyWasteInfo
			</p>
		</div>
	</body>
</html>
