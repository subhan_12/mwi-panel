<?php ob_start();	

require_once('includes/dbconnection.php');

session_start();
if(!isset($_SESSION['user_id']))
{
header('location:index.php');


}
$id =  $_GET['id'];
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>My Waste Info</title>
		<link rel="stylesheet" type="text/css" href="styles/index.css"/>

	</head>

<!-- TinyMCE -->
<script type="text/javascript" src="Tiny/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
	tinyMCE.init({
		// General options
		mode : "textareas",
		theme : "advanced",
		skin : "o2k7",
        skin_variant : "silver",
		height: "500",
		width: "745",

		document_base_url : "http://www.impactapps.com.au/",    
    	relative_urls : false, 
        remove_script_host : false,
		
		plugins : "jbimages,autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups,autosave",

		// Theme options
		language : "en",
		theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
		theme_advanced_buttons4 : "jbimages,|,insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,
		theme_advanced_background_colors : "FF00FF,FFFF00,000000",

		// Example word content CSS (should be your site CSS) this one removes paragraph margins
		content_css : "css/word.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",

		// Replace values for the template plugin
		template_replace_values : {
			username : "Some User",
			staffid : "991234",
		relative_urls : false
		}
	});
		
</script>

<script>

function validateForm(){	
var elm1 = tinyMCE.get('elm1').getContent();

if(elm1==""){
	alert("Enter the Content.")
	return false;
	}
}


</script>



	<body style="color:#fff;">
		
		<div class="logo_div">
		<div class="az_log">
			<img src="images/Waste-info-150-icon (1).png " width="75" height="75">
			
			<div style = "width: 300px; height: 100px; border: 0px solid red; margin-left: 400px; margin-top: -65px; font-size: 50px; font-weight: bold; color: #fff;">Waste Area</div>
			
			<div class="dash_logout">
				
				<span style = ""><img src="images/Apps-session-logout-icon.png">&nbsp;<a href="php_scripts/logout.php">Logout</a></span>
			<br>
			<span style="margin-top:5px;display:inline-block;"><img src="images/Administrator-icon.png">&nbsp;Hello Admin!</span>
			</div>
			
		</div>	
			
			
		</div>	<!-- logo_div ends>-->
		<div class="wrapper">
			
		<div class="menu_div">
			
			<ul class="menu">
				<li class="nav" ><a  href="dashboard.php">Dashboard</a></li>
				<li class="nav" ><a  href="states.php">States</a></li>
				<li class="nav" style="background: url(images/green_bar2.png);"><a style="color:#fff" href="waste.php">Waste Area</a></li>
				<li class="nav"><a href="councils.php">Councils</a></li>
				<li class="nav"><a href="towns.php">Towns</a></li>
				<li class="nav"><a href="streets.php">Streets</a></li>
				<li class="nav" style="width:148px;hover:width:148px;"><a style="width:148px;" href="settings.php">Settings</a></li>
				
				
				
			</ul>
			
			
		</div>	<!-- menu_div ends>-->
			
		<div class="az-container">
				<div class="az-left-content">

					<div class="az-left_menu">

						<ul class="left-nav">

							<li class="left-list">
								<a>Information</a>
							</li>

							<li class="left-list"  >
								<a>Collection Details</a>
							</li>

							<li class="left-list" style="background: url(images/green_bar2.png);" >
								<a style="color:#fff" >Materials</a>
							</li>
							<li class="left-list" >
								<a>FAQs and News</a>
							</li>

						</ul>

					</div>
					<!--az-left_menu-->

				</div>
				<!--az-left-content-->

				<?php

				$result = mysql_query("SELECT * FROM wasteAreas WHERE id = '$_GET[id]'");
				
				while($row = mysql_fetch_array($result))
				
				{
				    
				$material = $row['collection_material'];
				
				
				 } ?>

				<div class="az-right-content">
					
					<center><h2 class="wa_info_head" style="color:#fff;">Materials</h2></center>
					
					
				<div class="wa_info_formarea" style="margin-left:35px;">
				<form name="waste_form" method="POST" action="php_scripts/material_script.php?id=<?php echo $id; ?>" style="margin:0px;" onsubmit="return validateForm()">
				<div style="padding-top:35px;">
				 <label>Content:</label><br /><br>
 				<textarea id="elm1" name="elm1" rows="15" cols="90" style="width:90%;" onfocus="pass()" ><?php echo $material; ?></textarea>				
					</div>
				
				<div style="">
				<input type="submit" name="submit" class="az-submit  az-btn" value="Save and Next" style="width:110px;margin-left:-3px"><a href="#" class="az-btn" style="width:40px;">Cancel</a>
				</div>
				</div>	<!--wa_info_formarea-->
				
					
					
				</div>
				<!--az-right-content ends-->

			</div>
			<!--az-container ends-->
			
		</div>	<!--wrapper ends>-->
		<div class="az-copyright">
			<p>
				Copyright &copy; MyWasteInfo
			</p>
		</div>
	</body>
</html>
