<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>My Waste Info</title>

		<link rel="stylesheet" type="text/css" href="styles/index.css"/>

	</head>

<script type="text/javascript">
function validateForm(){	
var username = document.forms["login_form"]["username"].value;
var password = document.forms["login_form"]["password"].value;

if(username == "" && password == ""){
document.forms["login_form"]["username"].style.border = "1px solid red";
document.forms["login_form"]["username"].style.background = "#F9AA9E url('images/error.png') 235px center no-repeat";

document.forms["login_form"]["password"].style.border = "1px solid red";
document.forms["login_form"]["password"].style.background = "#F9AA9E url('images/error.png') 235px center no-repeat";
return false;
}

if(username == ""){
document.forms["login_form"]["username"].style.border = "1px solid red";
document.forms["login_form"]["username"].style.background = "#F9AA9E url('images/error.png') 235px center no-repeat";
return false;
}

if(password == "" ){
document.forms["login_form"]["password"].style.border = "1px solid red";
document.forms["login_form"]["password"].style.background = "#F9AA9E url('images/error.png') 235px center no-repeat";
return false;
}	
}

function pass(){
document.forms["login_form"]["username"].style.border = "1px solid #635843";
document.forms["login_form"]["username"].style.background = "white";
}

function p(){
document.forms["login_form"]["password"].style.border = "1px solid #635843";
document.forms["login_form"]["password"].style.background = "white";
}
</script>
	<body>
		<div class="az-logo"><img src="images/Waste-info-150-icon (1).png" >
			
			
			
		</div>
		<div class="az-wrapper">
			<div class="az-internalwrapper">
				<div class="az-form">
					<h2>Login Here</h2>
					<form name="login_form" class="az-login" id="login" action="php_scripts/login_script.php" method="POST" onsubmit="return validateForm()">

						<label>Username</label>
						<input type="text" name="username" class="az-user" onfocus="pass()" style="border:1px solid black;" />
						<br>
						<label>Password</label>
						<input type="password" name="password" class="az-user" onfocus="p()" style="border:1px solid black;" />
						<br>
						<label></label>
						<input type="submit" name="submit" class="az-submit  az-btn" value="Login">
						<input type="reset" name="reset" class="az-submit classname az-btn" value="Clear"><span class="az-forget"><a href="forget.php">Forgot password?</a></span>

					</form>

				</div>
				<!--az-form ends-->
<?php
				if($_GET['m'])
				{ ?>

				<div style="color:#ffffff;text-shadow:none;margin-left:150px;margin-top:270px;">Wrong id or password.</div>
			<? }

				if($_GET['msgs'])
				{ ?>

				<div style="color:#ffffff;text-shadow:none;margin-left:150px;margin-top:270px;"><?php echo $_GET['msgs']; ?></div>
			<? } ?>
			</div>
			<!--az-internalwrapper ends-->

		</div>
		<!--az-wrapper ends-->

		<div class="az-copyright">
			<p>
				Copyright &copy; MyWasteInfo
			</p>
		</div>
	</body>
</html>
