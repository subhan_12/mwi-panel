<?php ob_start();	

require_once('includes/dbconnection.php');

$id =  $_GET['id'];
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>My Waste Info</title>
		<link rel="stylesheet" type="text/css" href="styles/index.css"/>

	</head>




	<body>
		
		<div class="logo_div">
		<div class="az_log">
			<img src="images/Waste-info-150-icon (1).png " width="75" height="75">
			
			<div style = "width: 300px; height: 100px; border: 0px solid red; margin-left: 400px; margin-top: -65px; font-size: 50px; font-weight: bold; color: #fff;">Waste Area</div>
			
			<div class="dash_logout">
				
				<span style = ""><img src="images/Apps-session-logout-icon.png">&nbsp;<a href="php_scripts/logout.php">Logout</a></span>
			<br>
			<span style="margin-top:5px;display:inline-block;"><img src="images/Administrator-icon.png">&nbsp;Hello Admin!</span>
			</div>
			
		</div>	
			
			
		</div>	<!-- logo_div ends>-->
		<div class="wrapper">
			
		<div class="menu_div">
			
			<ul class="menu">
				<li class="nav" ><a  href="dashboard.php">Dashboard</a></li>
				<li class="nav" ><a  href="states.php">States</a></li>
				<li class="nav" ><a  href="waste.php">Waste Area</a></li>
				<li class="nav" style="background: url(images/green_bar2.png);"><a style="color:#fff" href="councils.php">Councils</a></li>
				<li class="nav"><a href="towns.php">Towns</a></li>
				<li class="nav"><a href="streets.php">Streets</a></li>
				<li class="nav" style="width:148px;hover:width:148px;"><a style="width:148px;" href="settings.php">Settings</a></li>
				
				
				
			</ul>
			
			
		</div>	<!-- menu_div ends>-->
			
		<div class="az-container" style="">
			<a href="council_info.php" class="az-btn" style="margin:50px; margin-right:0;margin-left:60px;width:90px;">Add a Council</a>
			<a href="coun.php" class="az-btn" style="margin-left:10px;width:70px;">Council Info</a>
<div class="council_area">

<table class="first_conc" width="900" height="30" border="0" cellspacing="0">
  <tr>
    
    <td bgcolor="#E4E4E4" align="left" style="width:250px; border-bottom:solid 1px  #CCC;padding-left:10px;">Council Name</td>
    <td bgcolor="#E4E4E4" align="left" style="width:250px; border-bottom:solid 1px  #CCC;">Cont.No</td>
    <td bgcolor="#E4E4E4" align="left" style="width:250px; border-bottom:solid 1px  #CCC;">Cont.Email</td>
      <td bgcolor="#E4E4E4" align="right" style="width:65px;" ><img src=""/><a style="text-decoration:none; color:#000;" href="#"></a></td>
        <td bgcolor="#E4E4E4" align="right" style="width:65px;" ><img src=""/><a style="text-decoration:none; color:#000;" href="#"> </a></td>
    
  </tr>
</table> 

<a href="#" class="az-btn" style="margin:20px;margin-left:25px;width:120px;">Dependent Councils</a>

<?php

$id = $_GET[id];
$result = mysql_query("SELECT * FROM councilsData WHERE council_type = 'dependent'");

while($row = mysql_fetch_array($result))

{


?>
<table class="first_conc" width="900" height="30" border="0" cellspacing="0">
  <tr>
    
    <td bgcolor="#E4E4E4" align="left" style="width:250px; border-bottom:solid 1px  #CCC;padding-left:10px;"><?php echo $row['name']; ?></td>
    <td bgcolor="#E4E4E4" align="left" style="width:250px; border-bottom:solid 1px  #CCC;"><?php echo $row['number']; ?></td>
    <td bgcolor="#E4E4E4" align="left" style="width:250px; border-bottom:solid 1px  #CCC;"><?php echo $row['email']; ?></td>
      <td bgcolor="#E4E4E4" align="right" style="width:65px; border-bottom:solid 1px  #CCC;padding-left:10px;"><img src="images/edit_icon.png"/><a style="text-decoration:none; color:#000;" href="council_info.php?id=<?php echo $row['id']; ?>">edit</a></td>
        <td bgcolor="#E4E4E4" align="right" style="width:65px; border-bottom:solid 1px  #CCC;padding-right:10px;"><img src="images/delete_icon.png"/><a style="text-decoration:none; color:#000;" onclick="javascript:return confirm('Are you sure you want to delete ?')"  href="php_scripts/delete_councils.php?id=<?php echo $row['id']; ?>"> delete</a></td>
    
  </tr>
  

</table> 
<? }
?>

<a href="#" class="az-btn" style="margin:20px;margin-left:25px;width:130px;">Independent Councils</a>

<?php

$id = $_GET[id];
$result = mysql_query("SELECT * FROM councilsData WHERE council_type = 'independent'");

while($row = mysql_fetch_array($result))

{


?>
<table class="first_conc" width="900" height="30" border="0" cellspacing="0">
  <tr>
    
    <td bgcolor="#E4E4E4" align="left" style="width:250px; border-bottom:solid 1px  #CCC;padding-left:10px;"><?php echo $row['name']; ?></td>
    <td bgcolor="#E4E4E4" align="left" style="width:250px; border-bottom:solid 1px  #CCC;padding-left:10px;"><?php echo $row['number']; ?></td>
    <td bgcolor="#E4E4E4" align="left" style="width:250px; border-bottom:solid 1px  #CCC;padding-left:10px;"><?php echo $row['email']; ?></td>
      <td bgcolor="#E4E4E4" align="right" style="width:65px; border-bottom:solid 1px  #CCC;padding-left:10px;"><img src="images/edit_icon.png"/><a style="text-decoration:none; color:#000;" href="council_info.php?id=<?php echo $row['id']; ?>">edit</a></td>
        <td bgcolor="#E4E4E4" align="right" style="width:65px; border-bottom:solid 1px  #CCC; padding-right:10px;"><img src="images/delete_icon.png"/><a style="text-decoration:none; color:#000;" onclick="javascript:return confirm('Are you sure you want to delete ?')"  href="php_scripts/delete_councils.php?id=<?php echo $row['id']; ?>"> delete</a></td>
    
  </tr>
  

</table> 
<? }
?>

</div>
			
		</div>	<!--az-container ends-->
			
		</div>	<!--wrapper ends>-->
		<div class="az-copyright">
			<p>
				Copyright &copy; MyWasteInfo
			</p>
		</div>
	</body>
</html>
