<?php 

ob_start(); 
 
require_once('includes/dbconnection.php');

session_start();
if(!isset($_SESSION['user_id']))
{
header('location:index.php');
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>My Waste Info</title>
		<link rel="stylesheet" type="text/css" href="styles/index.css"/>
		<script type="text/javascript" src="js/validations.js"></script>
	</head>
<script type="text/javascript" src="jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
	tinyMCE.init({
		// General options		
		mode : "exact",
		elements :"area1",
		theme : "advanced",
		body_class : "elm1",
		skin : "o2k7",
        skin_variant : "silver",
		height: "400",
		width: "400",

		document_base_url : "http://www.impactapps.com.au/",    
    	relative_urls : false, 
        remove_script_host : false,
		
		plugins : "jbimages,autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups,autosave",

		// Theme options
		language : "en",
		theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
		theme_advanced_buttons4 : "jbimages,|,insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,
		theme_advanced_background_colors : "FF00FF,FFFF00,000000",

		// Example word content CSS (should be your site CSS) this one removes paragraph margins
		content_css : "css/word.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",

		// Replace values for the template plugin
		template_replace_values : {
			username : "Some User",
			staffid : "991234",
		relative_urls : false
		}
	});
		
</script>


	<body>
		
		<div class="logo_div">
		<div class="az_log">
			<img src="images/Waste-info-150-icon (1).png " width="75" height="75">
			
			<div style = "width: 300px; height: 100px; border: 0px solid red; margin-left: 420px; margin-top: -65px; font-size: 50px; font-weight: bold; color: #fff;">Towns</div>
			
			<div class="dash_logout">
				
				<span style = ""><img src="images/Apps-session-logout-icon.png">&nbsp;<a href="php_scripts/logout.php">Logout</a></span>
			<br>
			<span style="margin-top:5px;display:inline-block;"><img src="images/Administrator-icon.png">&nbsp;Hello Admin!</span>
			</div>
			
		</div>	
			
			
		</div>	<!-- logo_div ends>-->
		<div class="wrapper">
				
		<div clas
		<div class="menu_div">
			
			<ul class="menu">
				<li class="nav" ><a  href="dashboard.php">Dashboard</a></li>
				<li class="nav" ><a  href="states.php">States</a></li>
				<li class="nav"><a href="waste.php">Waste Area</a></li>
				<li class="nav"><a href="councils.php">Councils</a></li>
				<li class="nav" style=""><a  style="" href="towns.php" >Towns</a></li>
				<li class="nav"><a href="streets.php">Streets</a></li>
				<li class="nav" style="width:148px;hover:width:148px;background: url(images/green_bar2.png);"><a style="width:148px;color:#fff" href="settings.php">Settings</a></li>
				
	
			</ul>
			
			
		</div>	<!-- menu_div ends>-->
			
		<div class="az-container" style="">
        
        <div style="width:950px;min-height:100px;margin-left:200px;margin-top:100px;padding:0px;">
        <form name="login_form" class="az-login" id="login" action="php_scripts/password_script.php" method="POST" onsubmit="return settings()" style="">

						<label style="width:130px;">Old Password</label>
						<input type="password" name="old_pass" class="az-user" onfocus="oldp()" style="border:1px solid #635843;" />
						
						<br>
                       
						<label style="width:130px;">New Password</label> 
                        <input type="password" name="new_pass" class="az-user" onfocus="newp()" value="" style="border:1px solid #635843"/>
                       
						<br>
						<input type="submit" name="submit" class="az-submit  az-btn" value="Change Password" style="width:130px;margin-left:142px;">
                        <input type="submit" name="reset" class="az-submit  az-btn" value="Clear" style="width:100px;margin-left:0px;">
						

					</form>



        <div style = "color: red;width:300px; font-style: italic; margin-left:142px;margin-top:20px;">




<span style = "color: #fff; display:inline-block;width:280px;font-style: italic;font-size:14px;">
<?php 
if(isset($_GET['msg'])){

if($_GET['msg']==0){
echo "Password change unsuccessful!";	
}
}
 
?>
 
<?php 

if(isset($_GET['msg'])){

if($_GET['msg']==1){
echo "Password changed successfully!";	
}
}
 
?>
</span> 
</br>

</div>
 <hr style="width:400px; margin-left:70px;" color ="black" size="1">
       
<?php

				$result = mysql_query("SELECT * FROM faq");
				
				while($row = mysql_fetch_array($result))
				
				{
				    $text = $row['text'];
	
				 } ?>

 <form name="" id="login" action="php_scripts/fac_script.php" method="POST">
		<label style="width:130px; margin-left:60px; color:#fff;">Edit FAQ</label><br><br>
		<textarea id="area1" name="area1"><?php echo $text; ?></textarea>
<input type="submit" name="submit" class="az-submit  az-btn" value="Submit" style="width:70px;">
</form>
        </div>
        
        
        
        
        
        
			


		
			
			
			
		</div>	<!--az-container ends-->
			
		</div>	<!--wrapper ends>-->
		<div class="az-copyright">
			<p>
				Copyright &copy; MyWasteInfo
			</p>
		</div>
	</body>
</html>
