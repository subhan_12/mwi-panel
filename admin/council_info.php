<?php ob_start();	

require_once('includes/dbconnection.php');

session_start();
if(!isset($_SESSION['user_id']))
{
header('location:index.php');


}
$id =  $_GET['id'];

$council_type = 1;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>My Waste Info</title>
		<link rel="stylesheet" type="text/css" href="styles/index.css"/>
<script type="text/javascript" src="js/validations.js"></script>

	</head>

<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Tangerine">


<!-- TinyMCE -->
<script type="text/javascript" src="Tiny/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
	tinyMCE.init({
		// General options
		mode : "exact",
		elements :"area1",
		theme : "advanced",
		skin : "o2k7",
        skin_variant : "silver",
		height: "200",
		width: "500",
		
		plugins : "jbimages,autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups,autosave",

		// Theme options
		language : "en",
		theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
		theme_advanced_buttons4 : "jbimages,|,insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,
		theme_advanced_background_colors : "FF00FF,FFFF00,000000",

		// Example word content CSS (should be your site CSS) this one removes paragraph margins
		content_css : "css/word.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",

		// Replace values for the template plugin
		template_replace_values : {
			username : "Some User",
			staffid : "991234",
		relative_urls : false
		}
	});
		
</script>


<script>
function disable(){

var check = document.forms["login_form"]["council_type"].value;
if (check == "Independent"){

 document.getElementById("waste_area").disabled = true;
}else if(check == "Dependent") {

 document.getElementById("waste_area").disabled = false;
}
}

</script>
<script>

function validateForm(){

var state = document.forms["login_form"]["state_tag"].value;

var council_type = document.forms["login_form"]["council_type"].value;

var waste_area = document.forms["login_form"]["waste_area"].value;

var c_name = document.forms["login_form"]["c_name"].value;

var c_person = document.forms["login_form"]["c_person"].value;

var c_no = document.forms["login_form"]["c_no"].value;

var c_email = document.forms["login_form"]["c_email"].value;

var c_towns = document.forms["login_form"]["c_towns"].value;

var info = document.forms["login_form"]["description"].value;

var file = document.forms["login_form"]["file"].value;

var chargers = document.forms["login_form"]["area1"].value;



if(state == "null" && council_type == "null" && waste_area == "null" && c_name.trim() == "" && c_person == "" && c_no == "" && c_email == "" && c_towns == "" && info == "" && file == "" && chargers == "") { 

alert("Enter the Required Fields.");
return false;
}

else if(council_type == "null" && waste_area == "null" && c_name.trim() == "" && c_person == "" && c_no == "" && c_email == "" && c_towns == "" && info == "" && file == "" && chargers == "") { 

alert("Enter the Required Fields.");
return false;
}

else if(waste_area == "null" && c_name.trim() == "" && c_person == "" && c_no == "" && c_email == "" && c_towns == "" && info == "" && file == "" && chargers == "") { 

alert("Enter the Required Fields.");
return false;
}

else if(c_name.trim() == "" && c_person == "" && c_no == "" && c_email == "" && c_towns == "" && info == "" && file == "" && chargers == "") { 

alert("Enter the Required Fields.");
return false;
}

else if(c_person == "" && c_no == "" && c_email == "" && c_towns == "" && info == "" && file == "" && chargers == "") { 

alert("Enter the Required Fields.");
return false;
}

else if(c_no == "" && c_email == "" && c_towns == "" && info == "" && file == "" && chargers == "") { 

alert("Enter the Required Fields.");
return false;
}

else if(c_email == "" && c_towns == "" && info == "" && file == "" && chargers == "") { 

alert("Enter the Required Fields.");
return false;
}

else if(c_towns == "" && info == "" && file == "" && chargers == "") { 

alert("Enter the Required Fields.");
return false;
}

else if(info == "" && file == "" && chargers == "") { 

alert("Enter the Required Fields.");
return false;
}
else if(file == "" && chargers == "") { 

alert("Enter the Required Fields.");
return false;
}

if(state == "null") { 

alert("Enter the State.");
return false;
}


if(c_name.trim() == "") { 

alert("Enter the Council Name.");
return false;
}

if(c_person == "") { 

alert("Enter the Contact Person.");
return false;
}

if(c_no == "") { 

alert("Enter the Contact Number.");
return false;
}

if(c_email == "") { 

alert("Enter the Contact Email.");
return false;
}

if(c_towns == "") { 

alert("Enter the number Towns.");
return false;
}

if(info == "") { 

alert("Enter the Council Info.");
return false;
}

if(charges == "") { 

alert("Enter the Annual Charges.");
return false;
}
}
</script>
	<body>
		
		<div class="logo_div">
		<div class="az_log">
			<img src="images/Waste-info-150-icon (1).png " width="75" height="75">
			
			<div style = "width: 300px; height: 100px; border: 0px solid red; margin-left: 400px; margin-top: -65px; font-size: 50px; font-weight: bold; color: #fff;">Council</div>
			
			<div class="dash_logout">
				
				<span style = ""><img src="images/Apps-session-logout-icon.png">&nbsp;<a href="php_scripts/logout.php">Logout</a></span>
			<br>
			<span style="margin-top:5px;display:inline-block;"><img src="images/Administrator-icon.png">&nbsp;Hello Admin!</span>
			</div>
			
		</div>	
			
			
		</div>	<!-- logo_div ends>-->
		<div class="wrapper">
			
		<div class="menu_div">
			
			<ul class="menu">
				<li class="nav" ><a  href="dashboard.php">Dashboard</a></li>
				<li class="nav" ><a  href="states.php">States</a></li>
				<li class="nav"><a  href="waste.php">Waste Area</a></li>
				<li class="nav" style="background: url(images/green_bar2.png);"><a style="color:#fff;" href="councils.php">Councils</a></li>
				<li class="nav"><a href="towns.php">Towns</a></li>
				<li class="nav"><a href="streets.php">Streets</a></li>
				<li class="nav" style="width:148px;hover:width:148px;"><a style="width:148px;" href="settings.php">Settings</a></li>
				</
				
				
			</ul>
			
			
		</div>	<!-- menu_div ends>-->
			
		<div class="az-container">
				<div class="az-left-content">

					<div class="az-left_menu">

						<ul class="left-nav">

							<li class="left-list" style="background: url(images/green_bar2.png);">
								<a  style="color:#fff">Add a Council</a>
							</li>
						
							
							<li class="left-list" >
								<a>Collection Details</a>
							</li>
								<li class="left-list" >
								<a>Facilities & Fees</a>
							</li>
							<li class="left-list" >
								<a>Materials</a>
							</li>
							<li class="left-list" >
								<a>FAQs and News</a>
							</li>

						</ul>

					</div>
					<!--az-left_menu-->

				</div>
				<!--az-left-content-->

				<div class="az-right-content">
					
					<center><h2 class="wa_info_head">Information</h2></center>
					<?php
						
					$id =  $_GET['id'];

					$result = mysql_query("SELECT * FROM councilsData WHERE id ='$id'");
					
					while($row = mysql_fetch_array($result))
					
					{
					$state = $row['state'];
					
					$council_type = $row['council_type'];
					$wid = $row['waste_area'];
					$name = $row['name'];
					$person = $row['person'];
					$number = $row['number'];
					$email = $row['email'];
					$town = $row['town'];
                    $image =$row['logo1'];
					$description = $row['description'];
					$annual = $row['annual'];
					$website = $row['url'];
					
}

					$result_waste = mysql_query("SELECT * FROM wasteAreas WHERE id ='$wid'");
					
					while($row = mysql_fetch_array($result_waste))
					
					{
					$waste_area = $row['waste_area'];
}
					?>
					
				<div class="wa_info_formarea">


						<?php if($image) { ?>
						<img src="http://www.impactapps.com.au/files/council_logos/<?php echo $image; ?>" height="100" width="100" style="margin-right:10px; margin-top:-80px; float:right; " /><? } else { ?>
						<img src="images/dummy.png" height="100" width="100" style="margin-right:10px; margin-top:-80px; float:right; " /><? } ?>

				<form name="login_form" class="council-login" id="login" action="php_scripts/couninfo_script.php?id=<?php echo $id; ?>"  enctype="multipart/form-data" method="POST" onsubmit="return validateForm();">

						<label >State</label>
						<!--<input type="text" name="username" class="az-user" onfocus="pass()" style="border:1px solid black;" />-->
                        
                        
                        <select class="council_select" name="state_tag" id="state_tag" onclick = "state();">
						<option value="null" selected="selected">Select</option>
						<?php 
							  $result1= mysql_query("SELECT * FROM states");
				
								while($row2 = mysql_fetch_array($result1))
				
									{
    								if($state == $row2['id']) {
        							$selected = "selected=\"selected\"";
									echo $selected;
									} else {
       								$selected = "";
									}
    								echo "<option value=\"".$row2['id']."\" $selected>".$row2['state']."</option>\n ";
	
								
				 } ?> 
                			  
						</select>
						<br>
						<label >Council Type</label>
						<select class="council_select" name="council_type" id="council_type" onclick = "return disable()" onfocus = "council();">
						<option value="null" selected="selected">Select</option>

						<?php 
							if (!isset($id)) {

									echo "<option value='Dependent'>Dependent</option>\n ";
									echo "<option value='Independent'>Independent</option>\n ";
								} else {
							  $result2= mysql_query("SELECT * FROM councilsData WHERE id = $id");
								while($row3 = mysql_fetch_array($result2))
									{
									switch ($council_type)
									{
									case "Dependent":
										echo "<option value='Dependent' selected = 'selected'>Dependent</option>\n ";
										echo "<option value='Independent'>Independent</option>\n ";
									  break;
									case "Independent":
										echo "<option value='Independent' selected = 'selected'>Independent</option>\n ";
										echo "<option value='Dependent'>Dependent</option>\n ";
									  break;
									default:
									
									echo "<option value='Dependent'>Dependent</option>\n ";
									echo "<option value='Independent'>Independent</option>\n ";

									}
									}
									}
									
								
									?>	
 
                         
							  
						</select>
						
                        <br>
						<label >Waste Area</label>
						<select class="council_select" name="waste_area" id="waste_area"  onclick = "return waste();">
						<option value="null" selected="selected">Select</option>
						<?php 
							  $result4= mysql_query("SELECT * FROM wasteAreas");
				
								while($row4 = mysql_fetch_array($result4))
				
									{
    								if($waste_area == $row4['waste_area']) {
        							$selected = "selected=\"selected\"";
									echo $selected;
									} else {
       								$selected = "";
									}
    								echo "<option value=\"".$row4['id']."\" $selected>".$row4['waste_area']."</option>\n ";
	
									
				 } ?> 
                        
						</select>
						<br>
                        <label >Council Name</label>
						<input type="text" name="c_name" class="az-user" onfocus="cname()" value="<?php echo $name; ?> "  style="border:1px solid #635843"/>
                       <br> 
                        <label >Contact Person</label>
						<input type="text" name="c_person" class="az-user" onfocus="cp()" value="<?php echo $person; ?>" style="border:1px solid #635843"/>
                       <br> 
						<label >Contact Number</label> 
                        <input type="text" name="c_no" class="az-user" onfocus="cn()"value="<?php echo $number; ?>" style="border:1px solid #635843"/>
                       <br>     
                      <label >Contact Email</label> 
                        <input type="text" name="c_email" class="az-user" onfocus="ce()" value="<?php echo $email; ?>" style="border:1px solid #635843"/>
                       <br> 
						<label >Council Website</label> 
                        <input type="text" name="c_website" class="az-user" onfocus="ce()" value="<?php echo $website; ?>" style="border:1px solid #635843"/>
                       <br>
                        <label >Number of Towns</label> 
                        <input type="text" name="c_towns" class="az-user" onfocus="nt()" value="<?php echo $town; ?>" style="border:1px solid #635843"/>
                       <br>
						<div style=" float:left; margin-left:75px; color:#fff;">Council Info</div> 
                        <textarea name="description"  onfocus="pass()" cols="29" rows="5" style="margin-left:7px; resize:none;" ><?php echo $description; ?></textarea>
                       <br>
						
						<label>Council Logo</label> 
                        <input type="file" name="file" class="az-user " onclick = "pic();" style="width:85px;"/>
                       <br>
			  				<label>Annual Charges</label>
                        <textarea  name="area1" id="area1" onfocus="pass()" ><?php echo $annual; ?></textarea>	
						
						<input type="submit" name="submit" class="az-submit  az-btn" value="Save & Next" style="width:100px;margin-left:20px;">
						<input type="reset" name="reset" class="az-submit classname az-btn" value="Clear">

					</form>
					
				</div>	<!--wa_info_formarea-->
					
					
					
				</div>
				<!--az-right-content ends-->

			</div>
			<!--az-container ends-->
			
		</div>	<!--wrapper ends>-->
		<div class="az-copyright">
			<p>
				Copyright &copy; MyWasteInfo
			</p>
		</div>
	</body>
</html>
