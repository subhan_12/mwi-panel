<?php ob_start();	

require_once('includes/dbconnection.php');


$id =  $_GET['id'];
$cid  = $_GET['cid'];

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>My Waste Info</title>
		<link rel="stylesheet" type="text/css" href="styles/index.css"/>

	</head>

<!-- TinyMCE -->

<script>

function validateForm(){	
var elm1 = tinyMCE.get('area1').getContent();
var elm2 = tinyMCE.get('area2').getContent();
var elm3 = tinyMCE.get('elm3').getContent();
var elm4 = tinyMCE.get('elm4').getContent();

if(elm2==""){
	alert("Enter the Content.")
	return false;
	}
}


</script>

	<body style="color:#fff;">
		
		<div class="logo_div">
		<div class="az_log">
			<img src="images/Waste-info-150-icon (1).png " width="75" height="75">
			
			<div style = "width: 300px; height: 100px; border: 0px solid red; margin-left: 400px; margin-top: -65px; font-size: 50px; font-weight: bold; color: #fff;">Waste Area</div>
				
			<div class="dash_logout">
				
				<span style = ""><img src="images/Apps-session-logout-icon.png">&nbsp;<a href="php_scripts/logout.php">Logout</a></span>
			<br>
			<span style="margin-top:5px;display:inline-block;"><img src="images/Administrator-icon.png">&nbsp;Hello Admin!</span>
			</div>
			
		</div>	
			
			
		</div>	<!-- logo_div ends>-->
		<div class="wrapper">
			
		<div class="menu_div">
			
			<ul class="menu">
				<li class="nav" ><a  href="dashboard.php">Dashboard</a></li>
				<li class="nav" ><a  href="states.php">States</a></li>
				<li class="nav" ><a  href="waste.php">Waste Area</a></li>
				<li class="nav" style="background: url(images/green_bar2.png);"><a style="color:#fff" href="councils.php">Councils</a></li>
				<li class="nav"><a href="towns.php">Towns</a></li>
				<li class="nav"><a href="streets.php">Streets</a></li>
				<li class="nav" style="width:148px;hover:width:148px;"><a style="width:148px;" href="settings.php">Settings</a></li>
				
				
				
			</ul>
			
			
		</div>	<!-- menu_div ends>-->
			
		<div class="az-container">
				<div class="az-left-content">

					<div class="az-left_menu">

						<ul class="left-nav">

							<li class="left-list">
								<a>Information</a>
							</li>

							<li class="left-list"  >
								<a >Collection Details</a>
							</li>
<li class="left-list" style="background: url(images/green_bar2.png);" >
								<a style="color:#fff">Facilities & Fees</a>
							</li>

							<li class="left-list" >
								<a>Materials</a>
							</li>
							<li class="left-list" >
								<a>FAQs and News</a>
							</li>

						</ul>

					</div>
					<!--az-left_menu-->

				</div>
				<!--az-left-content-->

				<div class="az-right-content" style="min-height:1580px;">
					
					<center><h2 class="wa_info_head" style="color:#fff;margin-bottom:50px;">Facilities & Fees</h2></center>
					

<form name="login_form" method="POST" action="php_scripts/facility_edit.php?id=<?php echo $id; ?>" style="margin:0px;" onsubmit="return validateForm()">

    <div style="margin:auto; width:750px;"><label style="float:left;padding-right:10px; margin-top:5px;color:#fff">Enter Facility:</label>
    <input type="text" id="title" name="facility" value="<?php echo $fac; ?>" style="padding:5px; width:250px; height:15px; border:1px solid black;" maxlength="50" onfocus="pass()" />
    <input type="submit" value="Save" class="az-btn" style="margin-left:10px;width:90px;" >
     <input type="reset" value="Clear" class="az-btn" style="margin-left:0px;width:80px;" >
    </div><br />


</form>
  
   
		<a href="facilities.php?id=<?php echo $_GET[id]; ?>" class="az-btn" style="width:30px;margin:10px;margin-left:29px;">Next</a>	
		<a href="council_fac.php" class="az-btn" style="width:70px;margin:10px;margin-left:10px;">Add Facility</a>	
					
				</div>
				<!--az-right-content ends-->

			</div>
			<!--az-container ends-->
			
		</div>	<!--wrapper ends>-->
		<div class="az-copyright">
			<p>
				Copyright &copy; MyWasteInfo
			</p>
		</div>
	</body>
</html>
