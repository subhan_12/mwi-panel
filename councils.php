<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-40855148-1', 'impactapps.com.au');
ga('send', 'pageview');

</script>
		<title>Waste Info</title>

		<link rel="stylesheet" type="text/css" href="styles/theme.css"/>

	</head>
	<body>
		<div class="wrapper">
			
		<div class="header">
			
			<div class="left_header">
			<a href="index.php"><img src="images/logo.png" /></a>
			</div>	<!--left_header ends-->
			
			<div class="right_header">
				<div class="menu_div">
					<ul class="menu">
				<li class="nav"><a  href="index.php" >Home</a></li>
				<li class="nav"><a href="councils.php" style="color:#1DA442;">Councils</a></li>
				<li class="nav"><a href="resident.php">Residents</a></li>
				<li class="nav" style="width:120px;"><a href="download_app.php" style="width:120px;">Download App</a></li>
				<li class="nav"><a href="about.php">About Us</a></li>
				<li class="nav"><a href="contact.php">Contact Us</a></li>
				
			</ul>
				</div>	<!--menu ends-->
				<div class="header_info">
					<h2>Councils</h2>
					<p></p>
					
					
				</div>	<!--header_info ends-->
				
				
			</div>	<!--right_header ends-->
			
		</div>	<!--header ends-->
		
		<div class="container">
			<div class="left_content">
				<div class="left_menu_div">
					<ul class="left-nav">

							<li class="left-list" style="background: url(images/green_bar2.png);width:210px;">
								<a  style="text-decoration:none;color:#fff;width:70px;hover:background:none;" href="index.php" onmouseover="this.style.background = 'none'">Home</a>
<span style="margin-left:-30px;display:inline-block;width:30px;color:#fff;">></span>
<a href="#" style="width:70px;text-decoration:underline;color:#fff;margin-left:-30px;" onmouseover="this.style.background = 'none'">Councils</a>
							</li>
<li class="left-list" >
								<a  href="technology_innov.php">> Technology Innovation</a>
							</li>
							<li class="left-list" >
								<a  href="saving_councils.php">> Saving Your Council Money</a>
							</li>
							<li class="left-list" >
								<a  href="customized_councils.php">> Customised To Your Council</a>
							</li>
							<li class="left-list" >
								<a  href="convenient_residents.php">> Convenient For Residents</a>
							</li>
							
							<li class="left-list" >
								<a  href="report_dumping.php">> Report Littering & Dumping</a>
							</li>
							
							<li class="left-list" >
								<a  href="enquiry.php">> Submit An Enquiry</a>
							</li>
							
							

						</ul>
				</div>
			</div>	<!--left_content ends-->
			
			
			<div class="center_content">
				<p class="tech_inno" style="padding:5px;margin:0px;padding-left:10px;padding-top:45px;">
The Waste Info App is the modern, fast and cost effective way to communicate with residents on their smartphones and tablets about waste management services in your area.					
					<br><br>
					
					
					
					
	<ul class="tech_inno_list" style="list-style-image:url('images/App-clean-icon.png');width:460px;">
					<li><a class="tech_inno_an">It can be customised for your council or waste area.</a></li>
					<li><a class="tech_inno_an">Handy reference guide on a range of issues including collection dates, what can be collected, charges, location of faculties and frequently asked questions.</a></li>
					<li><a class="tech_inno_an">Take the initiative by communicating directly with residents using current technology.</a></li>
					<li><a class="tech_inno_an">Over time reduce the reliance on printed media to convey collection times, permitted rubbish types and clean-ups.</a></li>
					<li><a class="tech_inno_an">Reduce missed collection costs with the help of the in-built reminder.</a></li>
					<li><a class="tech_inno_an">Empower residents to report unwelcome activities such as illegal dumping, overflowing bins or debris on roadways.</a></li>
					<li><a class="tech_inno_an">Provide critical news about upcoming clean-up events through the periodic update service.</a></li>
				
					
					
					
					</ul><br>				
					
				
					
					
				</p>
				
				<p class="tech_inno" style="">This App is available now.  <br><br><br><a href="contact.php" style="color:blue;text-decoration:none;">Contact us</a> to discuss how this could benefit your council and your residents.</p>
				
				
				
			</div>	<!--center_content ends-->
			<div class="right_content">
				<!--<img style="margin-top:40px;margin-left:55px;" src="images/offer_image.png">
				<img style="margin-top:30px;margin-left:55px;" src="images/green_basket.png">
				<img style="margin-top:30px;margin-left:55px;" src="images/yellow_basket.png">
				<img style="margin-top:30px;margin-left:55px;" src="images/red_basket.png">-->
			</div>	<!--right_content ends-->
			
			
			
			
		</div>	<!--container ends-->
			
		</div>	<!--wrapper ends-->
		
			
			<div class="footer_div">
					<ul class="menu_foo">
				<li class="nav"><a  href="index.php" style="color:#1DA442;">Home</a></li>
				<li class="nav"><a href="resident_faq.php">FAQs</a></li>
				<li class="nav"><a href="testimonial.php">Testimonials</a></li>
				<li class="nav" style="width:120px;"><a href="privacy.php" style="width:120px;">Privacy Policy</a></li>
				<li class="nav"><a href="enquiry.php" style="width:120px;">Submit An Enquiry</a></li>
				<span class="nav_foo" style="">Copyright &copy; 2013 Impact Apps</span>
				
			</ul>
		</div>
		
	</body>
	</html>