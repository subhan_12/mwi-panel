<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-40855148-1', 'impactapps.com.au');
ga('send', 'pageview');

</script>
		<title>Waste Info</title>

		<link rel="stylesheet" type="text/css" href="styles/theme.css"/>

	</head>
	<body>
		<div class="wrapper">
			
		<div class="header">
			
			<div class="left_header">
				<a href="index.php"><img src="images/logo.png" /></a>
			</div>	<!--left_header ends-->
			
			<div class="right_header">
				<div class="menu_div">
					<ul class="menu">
				<li class="nav"><a  href="index.php" >Home</a></li>
				<li class="nav"><a href="councils.php" style="color:#1DA442;">Councils</a></li>
				<li class="nav"><a href="resident.php">Residents</a></li>
				<li class="nav" style="width:120px;"><a href="download_app.php" style="width:120px;">Download App</a></li>
				<li class="nav"><a href="about.php">About Us</a></li>
				<li class="nav"><a href="contact.php">Contact Us</a></li>
				
			</ul>
				</div>	<!--menu ends-->
				<div class="header_info">
					<h2 style="">Customised To Your Council</h2>
					<p></p>
					
					
				</div>	<!--header_info ends-->
				
				
			</div>	<!--right_header ends-->
			
		</div>	<!--header ends-->
		
		<div class="container">
			<div class="left_content">
				<div class="left_menu_div">
					<ul class="left-nav">

							<li class="left-list" style="background: url(images/green_bar2.png);width:210px;">
								<a  style="text-decoration:underline;color:#fff;width:70px;hover:background:none;" href="index.php" onmouseover="this.style.background = 'none'">Home</a>

<span style="margin-left:-30px;display:inline-block;width:30px;color:#fff;">></span>
<a href="councils.php" style="width:70px;text-decoration:underline;color:#fff;margin-left:-30px;" onmouseover="this.style.background = 'none'">Councils</a>
							</li>
<li class="left-list" >
								<a  href="technology_innov.php">> Technology Innovation</a>
							</li>
							<li class="left-list" >
								<a  href="saving_councils.php" >> Saving Your Councils Money</a>
							</li>
							<li class="left-list" >
								<a  href="#" style="background:#949494;color:#fff">> Customised To Your Councils </a>
							</li>
							<li class="left-list" >
								<a  href="convenient_residents.php">> Convenient For Residents</a>
							</li>
							
							<li class="left-list" >
								<a  href="report_dumping.php">> Report Littering & Dumping</a>
							</li>
							
							<li class="left-list" >
								<a  href="enquiry.php">> Submit An Enquiry</a>
							</li>
							
							

						</ul>
				</div>
			</div>	<!--left_content ends-->
			
			
			<div class="center_content" >
				
				<p class="tech_inno" style="padding:5px;margin:0px;padding-left:10px;padding-top:45px;">
					The Waste Info App can be customised for specific councils or regional waste areas.
					<br><br>
					
When a user downloads the App and enters an address it will then load data specific to that address and its council area. For instance it will have all the relevant collection schedules, listings of accepted and non-accepted materials, collection facilities and contact details.<br>
					<br>
					The data content can be updated twice per year and this is included in the annual fee. This will allow Councils to update the App for any changes to collection dates or to update fees and charges relating to waste facilities.<br><br>
				Lastly, we can include news stories and important information in a news page on the App.
				</p>
				
				
				
			</div>	<!--center_content ends-->
			<div class="right_content">
				<img style="margin-top:40px;margin-left:55px;" src="images/offer_image.png">
				<img style="margin-top:10px;margin-left:75px;" src="images/iphone_image.png">
				
			</div>	<!--right_content ends-->
			
			
			
			
		</div>	<!--container ends-->
			
		</div>	<!--wrapper ends-->
		
			
			<div class="footer_div">
					<ul class="menu_foo">
				<li class="nav"><a  href="index.php" style="color:#1DA442;">Home</a></li>
				<li class="nav"><a href="resident_faq.php">FAQs</a></li>
				<li class="nav"><a href="testimonial.php">Testimonials</a></li>
				<li class="nav" style="width:120px;"><a href="privacy.php" style="width:120px;">Privacy Policy</a></li>
				<li class="nav"><a href="enquiry.php" style="width:120px;">Submit An Enquiry</a></li>
				<span class="nav_foo" style="">Copyright &copy; 2013 Impact Apps</span>
				
			</ul>
		</div>
		
	</body>
	</html>